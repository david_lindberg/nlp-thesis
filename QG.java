//package QG;

//import org.apache.poi.xwpf.extractor.*;
//import org.apache.poi.xwpf.usermodel.*;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Properties;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.util.*;
import edu.stanford.nlp.ling.CoreAnnotations.*;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefChain.CorefMention;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.*;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.PriorityQueue;

import jit.srl.*;
import jit.srl.plugins.SENNAPlugin;
import jit.generator.*;
import jit.sentence.Sentence;
import jit.util.StringUtils;
import jit.util.FormatConverter;
import jit.anaphora.PronounResolver;
import jit.ranking.FeatureCollector;
import jit.question.FeatureAdornedSRLQuestion;
import jit.glossary.Glossary;

public class QG
{
	//private static final String SENNA_CMD = "./senna -srl -pos -posvbs";
	private Generator gen;
        private Glossary glossary;
        private StanfordCoreNLP annotator_pipeline;
        private Properties annotation_properties;
        private SENNAPlugin srl_plugin;
        private HashMap<SRLSpan.Type, Integer> srl_count;
        private FeatureCollector feature_collector;
        private int template_use_count[];

        public QG()
        {
            initPipeline();
            srl_count = new HashMap<SRLSpan.Type, Integer>();
            feature_collector = new FeatureCollector();
        }

        public Generator getGenerator()
        {
            return gen;
        }

        private void initPipeline()
        {
            gen = new Generator("./templates", "./model_weights");
            template_use_count = new int[gen.templateCount()];
            glossary = new Glossary("glossaries/global warming");
            gen.setGlossary(glossary);
            //glossary.printGlossary();
            srl_plugin = new SENNAPlugin();
            annotation_properties = new Properties();

            annotation_properties.put("annotators","tokenize, ssplit, pos, lemma, ner, parse");
            annotator_pipeline = new StanfordCoreNLP(annotation_properties);

        }

	public Sentence[] generate(String[] lines, String[] orig_lines) throws IOException, FileNotFoundException, InterruptedException
	{
		
		ArrayList<String> questions = new ArrayList<String>();
		//System.out.println("Starting up generator...");
		//Generator g = new Generator("./templates");
		
		SENNAPlugin sp = new SENNAPlugin();
		Properties prep = new Properties();
                prep.put("annotators","tokenize, ssplit, pos, lemma, ner, parse");
		//prep.load(new FileInputStream("preprocess.properties"));
                //prep.setProperty("ner.model","lib/edu/stanford/nlp/models/ner/english.muc.7class.distsim.crf.ser.gz");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(prep);
		
		ArrayList<Sentence> sents = new ArrayList<Sentence>();
		
		for (int i = 0; i < lines.length; i++)
		{
			if ( lines[i].length() < 3)
				continue;
			
			System.out.println();
			System.out.println("-----------------------------");
			
			String line;
			//put the period back
			//lines[i] = lines[i]+".";
			
			line = lines[i];
			
			String[] tokens = line.split(" ");
			//System.out.println(tokens.length);
			
			
			// run corenlp on this sentence
			
			Annotation document = new Annotation(line);
			pipeline.annotate(document);
			
			List<CoreMap> sentences = document.get(SentencesAnnotation.class);
			CoreMap sentence = sentences.get(0);
			ArrayList<String> lemma = new ArrayList<String>();
			ArrayList<String> pos = new ArrayList<String>();
			ArrayList<String> ner = new ArrayList<String>();
			ArrayList<String> tok = new ArrayList<String>();
			//txt = new PrintWriter("emPronoun/tmp/tmp0.txt");
			
			/*
			 //System.out.println("<SENTENCE>");
			 for (CoreLabel token: sentence.get(TokensAnnotation.class))
			 {
			 //	System.out.println("<WORD>"+token.get(TextAnnotation.class)+"</WORD>");
			 //	System.out.println("<POS>"+token.get(PartOfSpeechAnnotation.class)+"</POS>");
			 //	System.out.println("<NER>"+token.get(NamedEntityTagAnnotation.class)+"</NER>");
			 }
			 //System.out.println("</SENTENCE>");
			 */
			
			///			Tree tree = sentence.get(TreeAnnotation.class);
			///			txt.println(tree.toString());
			///			txt.close();
			
			
			for (CoreLabel token : sentence.get(TokensAnnotation.class))
			{
				lemma.add(token.get(LemmaAnnotation.class));
				//pos.add(token.get(PartOfSpeechAnnotation.class));
				ner.add(token.get(NamedEntityTagAnnotation.class));
				tok.add(token.get(TextAnnotation.class));
			}
			
			SentenceSRL sentence_srl = sp.getSRL(line);

                        accumulateSRLCounts(sentence_srl);

			for (String s : sentence_srl.getPOS())
				pos.add(s);
			
			Sentence sent = new Sentence(orig_lines[i],sentence_srl, pos, lemma, ner, tok);
                        sent.addGlossaryAnnotations(glossary.getGlossaryAnnotations(tok.toArray(new String[tok.size()])));
                        
			ArrayList<SRLAnnotation> srl_annotations = sentence_srl.getAnnotations();
			String[] tokenization = sentence_srl.getTokenization();
			
			System.out.println();
			//System.out.println("srl summary");
			
			///			System.out.println("------------------");
			
			///			System.out.println("number of verb frames: " + srl_annotations.size());
			for (int a = 0; a < srl_annotations.size(); a++)
			{
				System.out.println("- - - - - - - - - - - - - - - -");
				SRLAnnotation ann = srl_annotations.get(a);
				SRLSpan vrb = ann.getVerb();
				
				if (vrb == null)
					// something went wrong
					continue;
				
				Iterator<SRLSpan.Type> itr = ann.getRoles().keySet().iterator();
				///				System.out.println("verb start: " + vrb.getStart());
				///				System.out.println("verb end: " + vrb.getEnd());
				String verb_text = (vrb.getStart() == vrb.getEnd()) ?
				tokenization[vrb.getStart()] : 
				concatStringArray(Arrays.copyOfRange(tokenization,vrb.getStart(),vrb.getEnd()+1)," ");
				System.out.println(vrb.getType() + ": " + verb_text + " tokens "+ (vrb.getEnd()-vrb.getStart()+1)+", depth: "+ ann.getPredicateDepth());;
				//System.out.println(concatStringArray(verb_text," "));
				//System.out.println(vrb.getType() + ": " + Arrays.copyOfRange(tokens, vrb.getStart(), vrb.getEnd()+1).toString());
				//System.out.println(vrb.getType() + ": " + vrb.getStart() + " - " + vrb.getEnd());
				while ( itr.hasNext())
				{
					ArrayList<SRLSpan> role = ann.getRole(itr.next());
					System.out.print(role.get(0).getType()+": ");
					
					for (int j = 0; j < role.size(); j++)
					{
						///						if (role.get(j).hasNestedVerb())
						///							System.out.print("nested: ");
						///						else 
						///							System.out.print("non nested: ");
						
						//System.out.println(role.get(j).isPlural());
						System.out.print("[");
						String role_text = (role.get(j).getStart() == role.get(j).getEnd()) ?
						tokenization[role.get(j).getStart()] :
						concatStringArray(Arrays.copyOfRange(tokenization,role.get(j).getStart(),role.get(j).getEnd()+1)," ");
						
						//System.out.println(role.getType() + ": " + role.getStart() + " - " + role.getEnd());
						//System.out.println(role.get(j).getType()+": "+role_text);
						System.out.print(role_text+"]");
					}
					System.out.println();
				}
			}
			
			PriorityQueue<FeatureAdornedSRLQuestion> queue = gen.generateQuestions(sent, feature_collector);
			System.out.println("# generated " + queue.size());
			//for (FeatureAdornedSRLQuestion s : q)
			while (!queue.isEmpty())
                        {
                                FeatureAdornedSRLQuestion s = queue.poll();
				System.out.println(s.getText() + " " + s.getScore());
				if (!questions.contains(s.getText()))
				{
                                        template_use_count[s.getTemplateID()]++;
					questions.add(s.getText());
					sent.addQuestion(s);
				}
			}
			
			sents.add(sent);
		}	

                for (int count : template_use_count)
                    System.out.print(count+" ");

                printSRLCounts();
                resetSRLCounts();
		return sents.toArray(new Sentence[sents.size()]);
	} 
	
	public static void main(String[] args) throws IOException, FileNotFoundException, InterruptedException
	{
                //System.out.println(Runtime.getRuntime().maxMemory());
		///ArrayList<String> questions = new ArrayList<String>();
		//System.out.println("Starting up generator...");
		///Generator g = new Generator("./templates");
		
		/* MOVED TO jit.util.FormatConverter
		 
		File file = new File(args[0]);
		XWPFDocument doc = new XWPFDocument(new FileInputStream(file));
		XWPFWordExtractor wordExtractor = new XWPFWordExtractor(doc);
		String text = wordExtractor.getText();
		PrintWriter txt = new PrintWriter(args[0]+".txt");
		txt.println(text);
		txt.close();
		*/
		
		/* moved to jit.anaphora.PronounResolver
		PrintWriter parse_out = new PrintWriter("emPronoun/tmp/tmp0.txt");
		Properties p = new Properties();
		p.put("annotators","tokenize, ssplit, parse");
		StanfordCoreNLP pipe = new StanfordCoreNLP(p);
		*/
		
		/* moved to jit.util.FormatConverter
		 
		ArrayList<String> tmp_lines = new ArrayList<String>();
		String[] main_blocks = splitString(text,'\n');
		if (main_blocks.length > 1)
		{
			tmp_lines.add(main_blocks[0]);
			for (int l = 1; l < main_blocks.length; l++)
			{
				String[] tmp = splitString(main_blocks[l],'.');
				for (String t : tmp)
					tmp_lines.add(t+".");
			}
		}
		String[] lines = tmp_lines.toArray(new String[tmp_lines.size()]);

		*/
		
		String[] lines = FormatConverter.getLines(args[0],"docx"); ////
		
		/*** BEGIN CUT HERE
		
		 **** moved to jit.anaphora.PronounResolver ****
		 
		// parse each line and send output to parse_out
		for (String line : lines)
		{
			if (line.length() < 3)
				continue;
			Annotation tree_doc = new Annotation(line);
			pipe.annotate(tree_doc);
			
			CoreMap sentence = tree_doc.get(SentencesAnnotation.class).get(0);
			
			parse_out.println(sentence.get(TreeAnnotation.class).toString());
		}
		parse_out.close();
		
		Process prn_res = Runtime.getRuntime().exec("sh pronouns.sh output");
		prn_res.waitFor();
		
		// read tree strings back into one string
		String trees = "";
		BufferedReader br = new BufferedReader(new FileReader("emPronoun/tmp/output"));
		String tree;
		while ((tree = br.readLine()) != null)
			trees+= tree+ '\n';
		
		HashMap<String, String> anaphora = new HashMap<String, String>();
		
		int start_index = 0;
		int found_index = trees.indexOf('#',start_index);
		ArrayList<String> repl = new ArrayList<String>();
		while (found_index != -1)
		{
//			if (found_index > 0)
//				System.out.println(trees.substring(found_index, trees.indexOf(' ', found_index)));
			
			start_index = found_index+1;
				// get subtree
			int string_start;
			for (string_start = found_index; trees.charAt(string_start)!='('; string_start--);
			
			int open_br = 1;
			int string_end;
			String number=new String();
			boolean found_number = false;
			for (string_end = found_index; open_br != 0; )
			{
				if (trees.charAt(string_end)==' ' && !found_number)
				{
					number = trees.substring(found_index, string_end);
					found_number=true;
				}
				if (trees.charAt(string_end)==')')
					open_br--;
				else if (trees.charAt(string_end)=='(')
					open_br++;
				
				string_end++;
			}
			
			String subtree = trees.substring(string_start, string_end);
			if (anaphora.get(number) == null)
				anaphora.put(number, subtree);
			else
			{ 
//				System.out.println("replacing: "+ subtree + " with " + anaphora.get(number));
				trees = trees.replace(subtree, anaphora.get(number));
			}
		

				// end get subtree
			
//			System.out.println(number + ": " + subtree);
			found_index = trees.indexOf('#',start_index);
		}
		
		lines = splitString(trees,'\n');
		for (int l = 0; l < lines.length; l++)
		{
			if (lines[l].length() < 3)
				continue;
			Tree t = Tree.valueOf(lines[l]);
			String sentence = "";
			for (Tree subtree : t)
				if (subtree.isLeaf())
				{
					// de-hyphenate names
					String val = subtree.label().value();
					
					int hyph_idx = val.indexOf('-');
					while (hyph_idx > 0 && hyph_idx < val.length()-1)
					{
						if (val.charAt(hyph_idx-1) != ' ' && val.charAt(hyph_idx+1) != ' ')
						{
							System.out.println(val);
							val = val.substring(0,hyph_idx)+" "+val.substring(hyph_idx+1);
							System.out.println(val);
						}
						hyph_idx = val.indexOf('-',hyph_idx+1);
					}

					sentence += val+" ";
				}
					///sentence += subtree.label().value()+" ";
			//String value = Tree.value(lines[l]);
			sentence = sentence.replaceAll("-LRB- ", "(");
			sentence = sentence.replaceAll("-RRB- ", ")");
			sentence = sentence.replaceAll("/"," and ");
			sentence = sentence.replaceAll("\\\\", "");
			lines[l] = sentence;
			System.out.println(lines[l]);
		}
		//System.out.println(lines.length);

		
		// END CUT HERE ****/
		
		//System.out.print(trees);
		
		lines = PronounResolver.resolve(lines);
		
		
		/*** moved to annotate() 
		
		SENNAPlugin sp = new SENNAPlugin();
		Properties prep = new Properties();
		prep.load(new FileInputStream("preprocess.properties"));
		StanfordCoreNLP pipeline = new StanfordCoreNLP(prep);
		
		for (int i = 0; i < lines.length; i++)
		{
			if ( lines[i].length() < 3)
				continue;
			
			System.out.println();
			System.out.println("-----------------------------");
			
			String line;
			//put the period back
			//lines[i] = lines[i]+".";
			
			line = lines[i];
			
			String[] tokens = line.split(" ");
			//System.out.println(tokens.length);
			
			
			// run corenlp on this sentence

			Annotation document = new Annotation(line);
			pipeline.annotate(document);
			
			List<CoreMap> sentences = document.get(SentencesAnnotation.class);
			CoreMap sentence = sentences.get(0);
			ArrayList<String> lemma = new ArrayList<String>();
			ArrayList<String> pos = new ArrayList<String>();
			ArrayList<String> ner = new ArrayList<String>();
			ArrayList<String> tok = new ArrayList<String>();
			//txt = new PrintWriter("emPronoun/tmp/tmp0.txt");


				
///			Tree tree = sentence.get(TreeAnnotation.class);
///			txt.println(tree.toString());
///			txt.close();
			
			
			for (CoreLabel token : sentence.get(TokensAnnotation.class))
			{
				lemma.add(token.get(LemmaAnnotation.class));
				//pos.add(token.get(PartOfSpeechAnnotation.class));
				ner.add(token.get(NamedEntityTagAnnotation.class));
				tok.add(token.get(TextAnnotation.class));
			}
			
			SentenceSRL sentence_srl = sp.getSRL(line);
			for (String s : sentence_srl.getPOS())
				pos.add(s);
			
			Sentence sent = new Sentence(sentence_srl, pos, lemma, ner, tok);			
			ArrayList<SRLAnnotation> srl_annotations = sentence_srl.getAnnotations();
			String[] tokenization = sentence_srl.getTokenization();
			
			System.out.println();
			//System.out.println("srl summary");

///			System.out.println("------------------");
			
///			System.out.println("number of verb frames: " + srl_annotations.size());
			for (int a = 0; a < srl_annotations.size(); a++)
			{
				System.out.println("- - - - - - - - - - - - - - - -");
				SRLAnnotation ann = srl_annotations.get(a);
				SRLSpan vrb = ann.getVerb();
				
				if (vrb == null)
					// something went wrong
					continue;
				
				Iterator<SRLSpan.Type> itr = ann.getRoles().keySet().iterator();
///				System.out.println("verb start: " + vrb.getStart());
///				System.out.println("verb end: " + vrb.getEnd());
				String verb_text = (vrb.getStart() == vrb.getEnd()) ?
									tokenization[vrb.getStart()] : 
									concatStringArray(Arrays.copyOfRange(tokenization,vrb.getStart(),vrb.getEnd()+1)," ");
				System.out.println(vrb.getType() + ": " + verb_text);
				//System.out.println(concatStringArray(verb_text," "));
				//System.out.println(vrb.getType() + ": " + Arrays.copyOfRange(tokens, vrb.getStart(), vrb.getEnd()+1).toString());
				//System.out.println(vrb.getType() + ": " + vrb.getStart() + " - " + vrb.getEnd());
				while ( itr.hasNext())
				{
					ArrayList<SRLSpan> role = ann.getRole(itr.next());
					System.out.print(role.get(0).getType()+": ");

					for (int j = 0; j < role.size(); j++)
					{
///						if (role.get(j).hasNestedVerb())
///							System.out.print("nested: ");
///						else 
///							System.out.print("non nested: ");
						
						//System.out.println(role.get(j).isPlural());
						System.out.print("[");
						String role_text = (role.get(j).getStart() == role.get(j).getEnd()) ?
										tokenization[role.get(j).getStart()] :
										concatStringArray(Arrays.copyOfRange(tokenization,role.get(j).getStart(),role.get(j).getEnd()+1)," ");
					
						//System.out.println(role.getType() + ": " + role.getStart() + " - " + role.getEnd());
						//System.out.println(role.getType()+": "+role_text);
						System.out.print(role_text+"]");
					}
					System.out.println();
				}
			}
			
			String [] q = g.generateQuestions(sent);

			for (String s : q)
			{
				System.out.println(s);
				if (!questions.contains(s))
				{
					questions.add(s);
					sent.addQuestion(s);
				}
			}

		}
		
		*********/
/*
		Sentence[] sents = annotate(lines, lines);
		
		// dump to file
		//PrintWriter pw = new PrintWriter(arg[0]+".out");
		
		System.out.println('\n'+"####################");

		int qCount = 0;
		for (int s = 0; s < sents.length; s++)
		{
			ArrayList<String> questions = sents[s].getQuestions();
			System.out.println("--------------------------------------");
			System.out.println('\n'+StringUtils.concatStringArray(lines[s].split(" ")," ")+'\n');
			//pw.println(StringUtils.concatStringArray(lines[s].split(" ")," "));
		//System.out.println(questions.size() + " questions");
		//System.out.println("############################################");
			System.out.println("Questions:");
			//pw.println("Questions:");
			for (String question : questions)
			{
				qCount++;
				System.out.println(question);
			}
		//System.out.println("############################################");
			System.out.println();
		}
		
		System.out.println("####################"+'\n'+qCount+ " questions generated"+'\n');
*/
	}
	
	private static String[] splitString(String str, char delim)
	{
		String[] tokens = str.split("["+delim+"]+");
		return tokens;
	}
	
	private static String concatStringArray(String[] array, String delim)
	{
		String out = array[0];
		if (array.length > 1)
			for (int i = 1; i < array.length; i++)
			{
				if (array[i].equals("."))
					out += array[i];
				else 
					out += delim + array[i];
			}
		return out;
	}

        private void accumulateSRLCounts(SentenceSRL srl)
        {
            for (SRLAnnotation ann : srl.getAnnotations())
            {
                for (SRLSpan.Type t : ann.getRoles().keySet())
                {
                    if (srl_count.containsKey(t))
                        srl_count.put(t, srl_count.get(t)+1);
                    else
                        srl_count.put(t, 1);
                }

            }
        }


        public void printSRLCounts()
        {
            System.out.println();
            System.out.println("distinct roles: "+ srl_count.keySet().size());
        }

        public void resetSRLCounts()
        {
            srl_count.clear();
        }
	// this will be moved into a SENNA plugin class and will return an SRLAnnotations array
/*
	private static void parseSRLOutput(String[] lines)
	{
		int rows = lines.length;
		int cols = splitString(lines[0], '\t').length;
		
		System.out.println(rows + " tokens and " + (cols-3) + " srl annotations");
	}
*/	
}


