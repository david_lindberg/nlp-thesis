/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * QGGUI.java
 *
 * Created on Dec 17, 2012, 3:06:48 PM
 */

/**
 *
 * @author dll4
 */

import javax.swing.JFileChooser;
import javax.swing.DefaultListModel;
import javax.activation.MimetypesFileTypeMap;
import java.io.File;

import jit.util.FormatConverter;
import jit.anaphora.PronounResolver;
import jit.sentence.Sentence;
import jit.question.FeatureAdornedSRLQuestion;

import java.io.PrintWriter;

public class QGGUI extends javax.swing.JFrame {

    private String[] lines;
    private String[] resolved_lines;
    private QG generator;
    private Sentence[] sentences;
    private String file_stem;
    /** Creates new form QGGUI */
    public QGGUI() {
        initComponents();
        generator = new QG();

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnOpen = new javax.swing.JButton();
        btnGenerate = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstQuestions = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstSentences = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("frmMain"); // NOI18N

        btnOpen.setText("Open");
        btnOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenActionPerformed(evt);
            }
        });

        btnGenerate.setText("Generate Questions");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        lstQuestions.setName(""); // NOI18N
        jScrollPane1.setViewportView(lstQuestions);

        lstSentences.setName(""); // NOI18N
        lstSentences.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstSentencesValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(lstSentences);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnOpen)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1))
                    .addComponent(btnGenerate, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOpen)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnGenerate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenActionPerformed
        final JFileChooser openFile = new JFileChooser();
        final DefaultListModel model = new DefaultListModel();

        if (openFile.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            lstSentences.clearSelection();
            lstSentences.setEnabled(false);
            String filename = openFile.getSelectedFile().getPath();
            file_stem = filename.substring(filename.lastIndexOf("/")+1,filename.lastIndexOf("."));
            System.out.println("filename: " + file_stem);
            String filetype = new MimetypesFileTypeMap().getContentType(new File(filename));
            System.out.println("Filetype is " + filetype);
            try
            {
               // PrintWriter pw = new PrintWriter("tmp/"+file_stem+".txt");
                lines = FormatConverter.getLines(filename,"docx");
                lstQuestions.setListData(new Object[0]);
                for (String line : lines)
                    if (line.length() > 2)
                    {
                        model.addElement(line);
                        
                   //     pw.println(line.toLowerCase());
                    }


             //   pw.close();
                //lstQuestions.removeAll();
                //lstSentences.removeAll();
                lstSentences.setModel(model);
                
            }
            catch (java.io.IOException e)
            {
                System.out.println("ERROR: " + "failed to open " + filename);
            }
        }
    }//GEN-LAST:event_btnOpenActionPerformed

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        // TODO add your handling code here:
        final DefaultListModel model = new DefaultListModel();

        try
        {
            lstQuestions.setListData(new Object[0]);
            lstSentences.clearSelection();
            resolved_lines = PronounResolver.resolve(lines);
            sentences = generator.generate(resolved_lines, lines);
            //lstQuestions.removeAll();
            

            //PrintWriter pw = new PrintWriter("output/"+file_stem+".full");
            PrintWriter question_pw = new PrintWriter("output/"+file_stem+".q");
            PrintWriter feature_pw = new PrintWriter("output/"+file_stem+".f");
            PrintWriter template_pw = new PrintWriter("output/"+file_stem+".t");
            PrintWriter match_pw = new PrintWriter("output/"+file_stem+".m");
            //PrintWriter tokens_pw = new PrintWriter("tmp/"+file_stem+".txt");
            //PrintWriter pw2 = new PrintWriter("output/"+file_stem+".features");
            for (Sentence s : sentences)
            {
//                pw.println(s.getText());
                for (String token : s.getSRL().getTokenization())
                {
//                    tokens_pw.print(token.toLowerCase()+" ");
                }
//                tokens_pw.println();
                match_pw.println(s.getQuestions().size());
                for (FeatureAdornedSRLQuestion q : s.getQuestions())
                {
                    model.addElement(q.getText());
//                    pw.println(q.getText()+ " f#:"+q.featureCount());
//                    pw.println(q.getFeatureVectorString());
                    question_pw.print(q.getText());
                    question_pw.println('\t'+q.getFeatureValueString());
                    template_pw.println(q.getText() + '\t'+q.getTemplateID());

                }
//                pw.println();
            }
//            tokens_pw.close();
            
//            pw.close();
            // dump out template usage to file
//            pw = new PrintWriter("output/"+file_stem+".stats");
//            pw.print(file_stem);
//            for (int count : generator.getGenerator().getTemplateUsageCount())
//                pw.print(" "+count);

//            pw.close();
            question_pw.close();
            feature_pw.close();
            template_pw.close();
            match_pw.close();
            lstQuestions.setModel(model);
            lstSentences.setEnabled(true);
            System.out.println(model.size());

        }
        catch (Exception e)
        {
            
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnGenerateActionPerformed

    private void lstSentencesValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstSentencesValueChanged
        // update lstQuestions to show only questions for selected sentence
        int index = lstSentences.getSelectedIndex();
        if (index != -1)
        {
            final DefaultListModel model = new DefaultListModel();

            for (FeatureAdornedSRLQuestion q : sentences[index].getQuestions())
            {
                model.addElement(q.getText()+ " " + q.getScore());
            }

            lstQuestions.setModel(model);
        }
    }//GEN-LAST:event_lstSentencesValueChanged

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new QGGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnOpen;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList lstQuestions;
    private javax.swing.JList lstSentences;
    // End of variables declaration//GEN-END:variables

}
