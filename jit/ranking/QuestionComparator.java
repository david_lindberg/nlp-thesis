/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jit.ranking;
import java.util.Comparator;
import jit.question.FeatureAdornedSRLQuestion;
/**
 *
 * @author david
 */
public class QuestionComparator implements Comparator
{
    public int compare(Object o1, Object o2)
    {
        FeatureAdornedSRLQuestion q1 = (FeatureAdornedSRLQuestion) o1;
        FeatureAdornedSRLQuestion q2 = (FeatureAdornedSRLQuestion) o2;

        double diff = q1.getScore() - q2.getScore();

        // this is a min-priority queue, so compare in reverse
        if (diff < 0.0)
            return 1;
        else if (diff == 0.0)
            return 0;
        else
            return -1;
    }
}
