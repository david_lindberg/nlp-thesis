/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jit.ranking;

import java.util.HashMap;
import java.util.EnumMap;
import java.util.Arrays;
/**
 *
 * @author david
 */
public class Ranker
{
    HashMap<FeatureCollector.FeatureLabel, Double> feature_weight;
    public Ranker(double[] weights)
    {
        feature_weight = new HashMap<FeatureCollector.FeatureLabel, Double>();
        int feature = 0;
        for (FeatureCollector.FeatureLabel f : FeatureCollector.FeatureLabel.values())
        {
            feature_weight.put(f, weights[feature]);
            feature++;
        }
    }
    
    public double score(EnumMap<FeatureCollector.FeatureLabel, Double> features)
    {

        double dot = 0.0;
        for (FeatureCollector.FeatureLabel f : features.keySet())
        {
            dot += features.get(f)*feature_weight.get(f);
        }


        return 1.0/(1.0+Math.exp(-1.0*dot));

                
    }



}
