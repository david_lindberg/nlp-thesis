/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jit.ranking;

/**
 *
 * @author dll4
 */

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.io.PrintWriter;
import jit.generator.Template;
import jit.question.SRLBasedQuestion;
import jit.sentence.Sentence;
import jit.srl.SRLAnnotation;
import jit.srl.SRLSpan;
import jit.util.StringUtils;

import jit.glossary.Glossary;
import edu.berkeley.nlp.lm.io.LmReaders;
import edu.berkeley.nlp.lm.NgramLanguageModel;

public class FeatureCollector
{
    private static final String[] ENTITIES = {"DATE", "DURATION", "LOCATION", "MISC",
                                    "NUMBER", "ORGANIZATION", "PERCENT",
                                    "PERSON", "SET"};

    public static enum FeatureLabel
    {
        //token count features -- entire source sentence -- DONE
        // INTEGER
        SOURCE_SENTENCE_TOKEN_COUNT,
        QUESTION_TOKEN_COUNT, //this gets thrown off by one or two depending on the number
                              //of punctuation characters (e.g. . and ?) in the template

        // language model features
        // REAL
        SOURCE_SENTENCE_LM_SCORE,
        //SOURCE_SENTENCE_LENGTH_NORMALIZED_LM_SCORE,
        QUESTION_LM_SCORE,
        //QUESTION_LENGTH_NORMALIZED_LM_SCORE,

        // named entity features -- DONE
        SOURCE_SENTENCE_HAS_DATE,
        SOURCE_SENTENCE_HAS_DURATION,
        SOURCE_SENTENCE_HAS_LOCATION,
        SOURCE_SENTENCE_HAS_MISC,
        SOURCE_SENTENCE_HAS_NUMBER,
        SOURCE_SENTENCE_HAS_ORGANIZATION,
        SOURCE_SENTENCE_HAS_PERCENT,
        SOURCE_SENTENCE_HAS_PERSON,
        SOURCE_SENTENCE_HAS_SET,

        QUESTION_HAS_DATE,
        QUESTION_HAS_DURATION,
        QUESTION_HAS_LOCATION,
        QUESTION_HAS_MISC,
        QUESTION_HAS_NUMBER,
        QUESTION_HAS_ORGANIZATION,
        QUESTION_HAS_PERCENT,
        QUESTION_HAS_PERSON,
        QUESTION_HAS_SET,

        // SRL token counts are for a particular predicate frame -- DONE
        SOURCE_FRAME_A0_TOKEN_COUNT,
        SOURCE_FRAME_A1_TOKEN_COUNT,
        SOURCE_FRAME_A2_TOKEN_COUNT,
        SOURCE_FRAME_A3_TOKEN_COUNT,
        SOURCE_FRAME_A4_TOKEN_COUNT,
        SOURCE_FRAME_AM_ADV_TOKEN_COUNT,
        SOURCE_FRAME_AM_CAU_TOKEN_COUNT,
        SOURCE_FRAME_AM_DIR_TOKEN_COUNT,
        SOURCE_FRAME_AM_DIS_TOKEN_COUNT,
        SOURCE_FRAME_AM_LOC_TOKEN_COUNT,
        SOURCE_FRAME_AM_MNR_TOKEN_COUNT,
        SOURCE_FRAME_AM_MOD_TOKEN_COUNT,
        SOURCE_FRAME_AM_NEG_TOKEN_COUNT,
        SOURCE_FRAME_AM_PNC_TOKEN_COUNT,
        SOURCE_FRAME_V_TOKEN_COUNT,
        SOURCE_FRAME_AM_TMP_TOKEN_COUNT,
        //SOURCE_FRAME_R_A0_TOKEN_COUNT,
        //SOURCE_FRAME_R_A1_TOKEN_COUNT,
        //SOURCE_FRAME_C_A1_TOKEN_COUNT,

        // some roles may be split over more than one span -- DONE
        // A1, TMP, DIS are
        // BOOLEAN
        //SOURCE_FRAME_A1_SPLIT,
        //SOURCE_FRAME_AM_DIS_SPLIT,
        //SOURCE_FRAME_AM_TMP_SPLIT,



        // predicate features
        // INTEGER
        //SOURCE_SENTENCE_PREDICATE_COUNT,

        // question-specific predicate features
        // INTEGER -- DONE
        PREDICATE_DEPTH,
        // BOOLEAN
        //PREDICATE_USED_IN_QUESTION, -- DONE
        /*
        PREDICATE_IS_VB,
        PREDICATE_IS_VBD,
        PREDICATE_IS_VBG,
        PREDICATE_IS_VBN,
        PREDICATE_IS_VBP,
        PREDICATE_IS_VBZ,
         * 
         */
        /*
        // template-specific features -- DONE
        TEMPLATE_REQUIRES_NONSLOT_A0,
        TEMPLATE_REQUIRES_NONSLOT_A1,
        TEMPLATE_REQUIRES_NONSLOT_A2,
        TEMPLATE_REQUIRES_NONSLOT_A3,
        TEMPLATE_REQUIRES_NONSLOT_A4,
        TEMPLATE_REQUIRES_NONSLOT_AM_ADV,
        TEMPLATE_REQUIRES_NONSLOT_AM_CAU,
        TEMPLATE_REQUIRES_NONSLOT_AM_DIR,
        TEMPLATE_REQUIRES_NONSLOT_AM_DIS,
        TEMPLATE_REQUIRES_NONSLOT_AM_LOC,
        TEMPLATE_REQUIRES_NONSLOT_AM_MNR,
        TEMPLATE_REQUIRES_NONSLOT_AM_MOD,
        TEMPLATE_REQUIRES_NONSLOT_AM_NEG,
        TEMPLATE_REQUIRES_NONSLOT_AM_PNC,
        TEMPLATE_REQUIRES_NONSLOT_AM_TMP,
        TEMPLATE_REQUIRES_NONSLOT_R_A0,
        TEMPLATE_REQUIRES_NONSLOT_R_A1,
        */
        /*
        // null features -- DONE
        TEMPLATE_REQUIRES_NULL_A0,
        TEMPLATE_REQUIRES_NULL_A1,
        TEMPLATE_REQUIRES_NULL_A2,
        TEMPLATE_REQUIRES_NULL_A3,
        TEMPLATE_REQUIRES_NULL_A4,
        TEMPLATE_REQUIRES_NULL_AM_ADV,
        TEMPLATE_REQUIRES_NULL_AM_CAU,
        TEMPLATE_REQUIRES_NULL_AM_DIR,
        TEMPLATE_REQUIRES_NULL_AM_DIS,
        TEMPLATE_REQUIRES_NULL_AM_LOC,
        TEMPLATE_REQUIRES_NULL_AM_MNR,
        TEMPLATE_REQUIRES_NULL_AM_MOD,
        TEMPLATE_REQUIRES_NULL_AM_NEG,
        TEMPLATE_REQUIRES_NULL_AM_PNC,
        TEMPLATE_REQUIRES_NULL_AM_TMP,
        */
        // template role text features -- DONE
        // NOTE: there is no TEMPLATE_REQUIRES_V feature, because templates ALWAYS require a verb
        // regardless of whether or not the verb text is used
        TEMPLATE_USES_A0_TEXT,
        TEMPLATE_USES_A1_TEXT,
        TEMPLATE_USES_A2_TEXT,
        TEMPLATE_USES_A3_TEXT,
        TEMPLATE_USES_A4_TEXT,
        TEMPLATE_USES_AM_ADV_TEXT,
        TEMPLATE_USES_AM_CAU_TEXT,
        TEMPLATE_USES_AM_DIR_TEXT,
        TEMPLATE_USES_AM_DIS_TEXT,
        TEMPLATE_USES_AM_LOC_TEXT,
        TEMPLATE_USES_AM_MNR_TEXT,
        TEMPLATE_USES_AM_MOD_TEXT,
        TEMPLATE_USES_AM_NEG_TEXT,
        TEMPLATE_USES_AM_PNC_TEXT,
        TEMPLATE_USES_V_TEXT,
        TEMPLATE_USES_AM_TMP_TEXT,
        //TEMPLATE_USES_R_A0_TEXT,
        //TEMPLATE_USES_R_A1_TEXT,
        // vagueness features

        // R_A0, R_A1 POS features -- NOT IMPLEMENTED
/*        SOURCE_SENTENCE_R_A0_IS_WDT,
        SOURCE_SENTENCE_R_A0_IS_WP,
        SOURCE_SENTENCE_R_A0_IS_WP$,
        SOURCE_SENTENCE_R_A1_IS_WDT,
        SOURCE_SENTENCE_R_A1_IS_WP,
        SOURCE_SENTENCE_R_A1_IS_WP$,
*/
        // glossary features -- DONE
        SOURCE_SENTENCE_GLOSS_TERM_COUNT,
        SOURCE_SENTENCE_AVG_GLOSS_TERM_ITO_DEGREE,
        QUESTION_GLOSS_TERM_COUNT,
        QUESTION_AVG_GLOSS_TERM_ITO_DEGREE
        /*
        // punctuation features -- DONE
        SOURCE_SENTENCE_PERCENT_NONTERMINAL_PUNC,
        QUESTION_PERCENT_NONTERMINAL_PUNC

         */
    }

    private EnumMap<FeatureLabel, Double> features;
    private NgramLanguageModel<String> lm;

    public FeatureCollector()
    {
        features = new EnumMap<FeatureLabel, Double>(FeatureLabel.class);
        //System.out.println("FEATURES: "+FeatureLabel.values().toString());
        LmReaders lm_reader = new LmReaders();
        System.out.print("reading language model...");
        lm = lm_reader.readLmBinary("./lm/wiki.3gram.binary");
        System.out.println(" done");
        
    }

    public EnumMap<FeatureLabel, Double> getFeatures()
    {
        return new EnumMap<FeatureLabel,Double>(features);
    }
    
    public void addFeature(FeatureLabel label, double value)
    {
        features.put(label, value);
    }

    public double getFeatureValue(FeatureLabel lbl)
    {
        return features.get(lbl);
    }

    public void collectFeatures(Sentence s, SRLBasedQuestion q, Template t, SRLAnnotation ann, Glossary gloss)
    {
        // this is going to get ugly

	String[] src_tok = StringUtils.tokenize(s.getText());//s.getSRL().getTokenization();//s.getTok().toArray(new String[s.getTok().size()]);
	String[] src_pos = s.getSRL().getPOS();
	String[] src_ner = s.getNER().toArray(new String[s.getNER().size()]);
        String[] q_tok = StringUtils.tokenize(q.getText());

        /********** GLOBAL TOKEN COUNT FEATURES **********/
        features.put(FeatureLabel.SOURCE_SENTENCE_TOKEN_COUNT, (double)src_tok.length);
        features.put(FeatureLabel.QUESTION_TOKEN_COUNT, (double)q_tok.length/*q.getTokenCount()*/);
        /********** END **********/

        /********** LANGUAGE MODEL FEATURES **********/

        features.put(FeatureLabel.SOURCE_SENTENCE_LM_SCORE, calcLmLogProb(src_tok));
        features.put(FeatureLabel.QUESTION_LM_SCORE, calcLmLogProb(q_tok));
        /********** END **********/
        /********** SOURCE SENTENCE NAMED ENTITY FEATURES *********/
        for (String ne_label : ENTITIES)
        {
            if (containsEntity(s.getNER(), ne_label))
            {
                features.put(FeatureLabel.valueOf("SOURCE_SENTENCE_HAS_"+ne_label), 1.0);
                // check if question also has a NE of this type
                if (questionContainsEntity(s, q, ne_label))
                    features.put(FeatureLabel.valueOf("QUESTION_HAS_"+ne_label), 1.0);
                else
                    features.put(FeatureLabel.valueOf("QUESTION_HAS_"+ne_label), 0.0);


            }
            else
            {
                //source sentence doesn't have an entity of this type
                features.put(FeatureLabel.valueOf("SOURCE_SENTENCE_HAS_"+ne_label), 0.0);
                //question can't have one either
                features.put(FeatureLabel.valueOf("QUESTION_HAS_"+ne_label), 0.0);


            }
        }
        /********** END **********/


        /********** PREDICATE FEATURES **********/
        features.put(FeatureLabel.PREDICATE_DEPTH, (double)ann.getPredicateDepth());
        /*
        if (t.hasSlot(SRLSpan.Type.V))
            features.put(FeatureLabel.PREDICATE_USED_IN_QUESTION, 1.0);
        else
            features.put(FeatureLabel.PREDICATE_USED_IN_QUESTION, 0.0);
        */
        // predicate pos features

        /*
        if (ann.getVerb() == null) // weird case where there is no predicate
        {
            features.put(FeatureLabel.PREDICATE_IS_VB, 0.0);
            features.put(FeatureLabel.PREDICATE_IS_VBD, 0.0);
            features.put(FeatureLabel.PREDICATE_IS_VBG, 0.0);
            features.put(FeatureLabel.PREDICATE_IS_VBN, 0.0);
            features.put(FeatureLabel.PREDICATE_IS_VBP, 0.0);
            features.put(FeatureLabel.PREDICATE_IS_VBZ, 0.0);
        }
        else
        {
            String vrb_pos = src_pos[ann.getVerb().getStart()];

            if (vrb_pos.equals("VB"))
            {
                features.put(FeatureLabel.PREDICATE_IS_VB, 1.0);
                features.put(FeatureLabel.PREDICATE_IS_VBD, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBG, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBN, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBP, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBZ, 0.0);
            }
            else if (vrb_pos.equals("VBD"))
            {
                features.put(FeatureLabel.PREDICATE_IS_VB, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBD, 1.0);
                features.put(FeatureLabel.PREDICATE_IS_VBG, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBN, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBP, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBZ, 0.0);
            }
            else if (vrb_pos.equals("VBG"))
            {
                features.put(FeatureLabel.PREDICATE_IS_VB, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBD, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBG, 1.0);
                features.put(FeatureLabel.PREDICATE_IS_VBN, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBP, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBZ, 0.0);
            }
            else if (vrb_pos.equals("VBN"))
            {
                features.put(FeatureLabel.PREDICATE_IS_VB, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBD, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBG, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBN, 1.0);
                features.put(FeatureLabel.PREDICATE_IS_VBP, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBZ, 0.0);
            }
            else if (vrb_pos.equals("VBP"))
            {
                features.put(FeatureLabel.PREDICATE_IS_VB, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBD, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBG, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBN, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBP, 1.0);
                features.put(FeatureLabel.PREDICATE_IS_VBZ, 0.0);
            }
            else if (vrb_pos.equals("VBZ"))
            {
                features.put(FeatureLabel.PREDICATE_IS_VB, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBD, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBG, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBN, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBP, 0.0);
                features.put(FeatureLabel.PREDICATE_IS_VBZ, 1.0);
            }
        }*/
        /********** END **********/

        /********** SRL FEATURES **********/

        int tok_count;
        SRLSpan current_span;

        String feature_label_str = FeatureLabel.values().toString();

        for (SRLSpan.Type role_type : SRLSpan.Type.values())
        {

            if (ann.getRole(role_type) != null)
            {
                current_span = ann.getRole(role_type).get(0);

                // verb text is special case, because a template always requires a verb
                if (role_type == SRLSpan.Type.V)
                {
                    if (t.hasSlot(role_type))
                        features.put(FeatureLabel.valueOf("TEMPLATE_USES_"+role_type.name()+"_TEXT"), 1.0);
                    else // text not used
                        features.put(FeatureLabel.valueOf("TEMPLATE_USES_"+role_type.name()+"_TEXT"), 0.0);

                }
                
                // token count
                if (featureExists("SOURCE_FRAME_"+role_type.name()+"_TOKEN_COUNT"))
                {

                    tok_count = current_span.getEnd() - current_span.getStart()+1;
                    features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_TOKEN_COUNT"), (double)tok_count);
                }

                // template

                // the template was used, so the role cannot be null in the template
                if (featureExists("TEMPLATE_REQUIRES_NULL_"+role_type.name()))
                {
                    features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_NULL_"+role_type.name()), 0.0);
                }

                // does the template use the text?
                if (t.hasSlot(role_type))
                {
                    // text is used and there can't be a nonslot prereq for this role
                    
                    if (featureExists("TEMPLATE_USES_"+role_type.name()+"_TEXT"))
                        features.put(FeatureLabel.valueOf("TEMPLATE_USES_"+role_type.name()+"_TEXT"), 1.0);
                    if (featureExists("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()))
                        features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()), 0.0);

                }
                else // role text not used
                {
                    if (featureExists("TEMPLATE_USES_"+role_type.name()+"_TEXT"))
                        features.put(FeatureLabel.valueOf("TEMPLATE_USES_"+role_type.name()+"_TEXT"), 0.0);
                    
                    // is ther a non-slot requirement?
                    if (t.hasNonSlotPrereq(role_type))
                    {
                        // this requirement cannot be null otherwise the template wouldn't have been used
                        if (featureExists("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()))
                            features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()), 1.0);
                    }
                    else
                    {
                        // template does not refer to this role at all
                        if (featureExists("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()))
                            features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()), 0.0);
                        
                    }
     
                    
                }
                /*
                if (featureExists("TEMPLATE_REQUIRES_"+role_type.name()))
                {
                    if (t.hasSlot(role_type) || t.hasNonSlotPrereq(role_type))
                    {
                        features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_"+role_type.name()), 1.0);
                        if (t.hasSlot(role_type)) // text is also used in the question
                            features.put(FeatureLabel.valueOf("TEMPLATE_USES_"+role_type.name()+"_TEXT"), 1.0);
                        else // text not used
                            features.put(FeatureLabel.valueOf("TEMPLATE_USES_"+role_type.name()+"_TEXT"), 0.0);

                    }
                    else // template neither requires nor uses
                    {

                        features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_"+role_type.name()), 0.0);
                        features.put(FeatureLabel.valueOf("TEMPLATE_USES_"+role_type.name()+"_TEXT"), 0.0);
                    }

                }

                */
                // split features
                if (featureExists("SOURCE_FRAME_"+role_type.name()+"_SPLIT"))
                {
                    features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_SPLIT"), (ann.getRole(role_type).size() > 1) ? 1.0 : 0.0);
                }

                // pos features
                if (featureExists("SOURCE_FRAME_"+role_type.name()+"_IS_WDT"))
                {
                    if (src_pos[current_span.getStart()].equals("WDT"))
                    {
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WDT"), 1.0);
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WP"), 0.0);
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WP$"), 0.0);
                    }
                    else if (src_pos[current_span.getStart()].equals("WP"))
                    {
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WDT"), 0.0);
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WP"), 1.0);
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WP$"), 0.0);

                    }
                    else if (src_pos[current_span.getStart()].equals("WP$"))
                    {
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WDT"), 0.0);
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WP"), 0.0);
                        features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WP$"), 1.0);

                    }
                }

            }
            else
            {
                // frame doesn't have the current role, so set features appropriately
                if (featureExists("SOURCE_FRAME_"+role_type.name()+"_TOKEN_COUNT"))
                    features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_TOKEN_COUNT"), 0.0);
            //    if (featureExists("TEMPLATE_REQUIRES_"+role_type.name()))
            //        features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_"+role_type.name()), 0.0);
                if (featureExists("TEMPLATE_USES_"+role_type.name()+"_TEXT"))
                    features.put(FeatureLabel.valueOf("TEMPLATE_USES_"+role_type.name()+"_TEXT"), 0.0);

                if (featureExists("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()))
                    features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()), 0.0);

                // template might require that this role not exist
                if (t.hasNonSlotPrereq(role_type))
                {
                //    if (t.hasNullRequirement(role_type)) // don't ned this check, it must have a null requirement
                //    {
                        if (featureExists("TEMPLATE_REQUIRES_NULL_"+role_type.name()))
                            features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_NULL_"+role_type.name()), 1.0);

                //    }

                }
                else // template does not refer to this role at all
                {
                    if (featureExists("TEMPLATE_REQUIRES_NULL_"+role_type.name()))
                        features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_NULL_"+role_type.name()), 0.0);
               //     if (featureExists("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()))
               //         features.put(FeatureLabel.valueOf("TEMPLATE_REQUIRES_NONSLOT_"+role_type.name()), 0.0);
                }

                     // split features
                if (featureExists("SOURCE_FRAME_"+role_type.name()+"_SPLIT"))
                {
                    features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_SPLIT"), 0.0);
                }

                // R_A0 and R_A1 have extra features we need to set to 0
                if (featureExists("SOURCE_FRAME_"+role_type.name()+"_IS_WDT"))
                {

                    features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WDT"), 0.0);
                    features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WP"), 0.0);
                    features.put(FeatureLabel.valueOf("SOURCE_FRAME_"+role_type.name()+"_IS_WP$"), 0.0);
                }
            }

        }

        /********** PUNCTUATION FEATURES **********/
        //features.put(FeatureLabel.SOURCE_SENTENCE_PERCENT_NONTERMINAL_PUNC, punctPercent(s.getText(),src_tok.length));
        //features.put(FeatureLabel.QUESTION_PERCENT_NONTERMINAL_PUNC, punctPercent(q.getText(), q.getTokenCount()));
        /********** END **********/

        /********** GLOSSARY FEATURES **********/

        // source sentence

        int[] glossary_annotations = gloss.getGlossaryAnnotations(src_tok);

        if (glossary_annotations == null)
        {
            // no glossary terms used
            features.put(FeatureLabel.SOURCE_SENTENCE_GLOSS_TERM_COUNT, 0.0);
            features.put(FeatureLabel.SOURCE_SENTENCE_AVG_GLOSS_TERM_ITO_DEGREE, 0.0);
        }
        else
        {
            features.put(FeatureLabel.SOURCE_SENTENCE_GLOSS_TERM_COUNT, (double)glossary_annotations.length);

            int tot_deg = 0;
            for (int idx : glossary_annotations) // for each glossary term found in sentence
            {
                tot_deg += gloss.inDegreeOf(idx)+gloss.outDegreeOf(idx);
            }
            features.put(FeatureLabel.SOURCE_SENTENCE_AVG_GLOSS_TERM_ITO_DEGREE, (double)tot_deg/glossary_annotations.length);
            
        }
        
        // question
        String[] q_tokens = q.getText().split("\\W|_");
        glossary_annotations = gloss.getGlossaryAnnotations(q_tokens);
        System.out.println("checking question for glossary features...");
        if (glossary_annotations == null)
        {
            // no glossary terms used
            features.put(FeatureLabel.QUESTION_GLOSS_TERM_COUNT, 0.0);
            features.put(FeatureLabel.QUESTION_AVG_GLOSS_TERM_ITO_DEGREE, 0.0);
        }
        else
        {
            features.put(FeatureLabel.QUESTION_GLOSS_TERM_COUNT, (double)glossary_annotations.length);

            int tot_deg = 0;
            for (int idx : glossary_annotations) // for each glossary term found in sentence
            {
                tot_deg += gloss.inDegreeOf(idx)+gloss.outDegreeOf(idx);
            }
            features.put(FeatureLabel.QUESTION_AVG_GLOSS_TERM_ITO_DEGREE, (double)tot_deg/glossary_annotations.length);

        }
        /********** END **********/
    }

    public void clearFeatures()
    {
        features.clear();
    }

    private boolean featureExists(String name)
    {
        try
        {
            FeatureLabel.valueOf(name);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    private boolean containsEntity(ArrayList<String> ne_array, String ne_type)
    {
        //System.out.println("looking for -- " + ne_type);
        for (String ne_token : ne_array)
        {
//            if (ne_token.length() > 2)
//                System.out.println(ne_token);
            if (ne_token.equals(ne_type))
                return true;
        }
        return false;
    }

    private boolean questionContainsEntity(Sentence s, SRLBasedQuestion q, String ne_label)
    {
        ArrayList<String> s_ner = s.getNER();
        ArrayList<String> s_tok = s.getTok();
        String q_text = q.getText();

        // interate through s_ner array looking for ne_label
        // if found, check if q_text contains the corresponding text from s_tok
        for (int i = 0; i < s_ner.size(); i++)
        {
            if (s_ner.get(i).equals(ne_label))
            {
                String search_text = s_tok.get(i);
                // in cases like "The Earth" both The and Earth are considered NEs,
                // so merge and look for combined tokens
                for (int j = i+1; j < s_ner.size() && s_ner.get(j).equals(ne_label); j++)
                    search_text += " "+s_tok.get(j);

                //System.out.println(q_text);
                search_text = StringUtils.genericPostprocess(search_text);
                //System.out.println(search_text+" in question?");
                if (q_text.toLowerCase().contains(search_text.toLowerCase()))
                {
                    //System.out.println("YES");
                    return true;
                }
               // else
                    //System.out.println("NO");
            }
        }
        return false;
    }

    private double punctPercent(String text, int tokenized_length)
    {
        int punct_count = 0;

        final char[] PUNCT = {';',':','\"', '\'', ','};

        for (char p : PUNCT)
        {
            punct_count += StringUtils.countOccurrences(text, p);
        }

        return punct_count/(double)tokenized_length;
    }

    private double calcLmLogProb(String[] line) // tokenized line
    {
        List<String> words = Arrays.asList(line);
        return lm.getLogProb(words);

    }


}
