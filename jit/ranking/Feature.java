/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jit.ranking;

/**
 *
 * @author david
 */

public class Feature
{
    private FeatureCollector.FeatureLabel label;
    private double value;

    public Feature(FeatureCollector.FeatureLabel lbl, double val)
    {
        setLabel(lbl);
        setValue(val);
    }

    public void setLabel(FeatureCollector.FeatureLabel lbl)
    {
        label = lbl;
    }

    public void setValue(double val)
    {
        value = val;
    }

    public FeatureCollector.FeatureLabel getLabel()
    {
        return label;
    }

    public double getValue()
    {
        return value;
    }
}
