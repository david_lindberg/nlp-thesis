package jit.sentence;

import jit.srl.SentenceSRL;
import jit.question.FeatureAdornedSRLQuestion;

import java.util.ArrayList;

public class Sentence
{
	private SentenceSRL srl;
	private ArrayList<String> pos;
	private ArrayList<String> lemma;
	private ArrayList<String> ner;
	private ArrayList<String> tok;
	private ArrayList<FeatureAdornedSRLQuestion> questions;
        //private PriorityQueue<FeatureAdornedSRLQuestion> questions;
        private String text;
        private int[] glossary_terms; // indices into glossary

	public Sentence(String txt, SentenceSRL srl, ArrayList<String> pos, ArrayList<String> lemma, ArrayList<String> ner,
					ArrayList<String> tok)
	{
                this.text = txt;
		this.srl = srl;
		this.pos = new ArrayList<String>(pos);
		this.lemma = new ArrayList<String>(lemma);
		this.ner = new ArrayList<String>(ner);
		this.tok = new ArrayList<String>(tok);
		questions = new ArrayList<FeatureAdornedSRLQuestion>();
	}

        public String getText()
        {
            return text;
        }

	public SentenceSRL getSRL()
	{
		return srl;
	}
	
	public String getPOSAt(int index)
	{
		return pos.get(index);
	}
	
	public String getLemmaAt(int index)
	{
		return lemma.get(index);
	}
	
	public String getNERAt(int index)
	{
		return ner.get(index);
	}
	
	public String getTokAt(int index)
	{
		return tok.get(index);
	}
	
	public ArrayList<String> getPOS()
	{
		return pos;
	}
	
	public ArrayList<String> getLemma()
	{
		return lemma;
	}
	
	public ArrayList<String> getNER()
	{
		return ner;
	}
	
	public ArrayList<String> getTok()
	{
		return tok;
	}
	
	public ArrayList<FeatureAdornedSRLQuestion> getQuestions()
	{
		return questions;
	}
	
	public void addQuestion(FeatureAdornedSRLQuestion q)
	{
		questions.add(q);
	}

        public void addGlossaryAnnotations(int[] annotations)
        {
            glossary_terms = annotations;
        }
}