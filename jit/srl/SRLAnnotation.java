package jit.srl;

import jit.srl.SRLSpan;

import java.util.ArrayList;
import java.util.HashMap;

public class SRLAnnotation
{
	private SRLSpan verb;
	//private ArrayList<SRLSpan> roles;
	private HashMap<SRLSpan.Type, ArrayList<SRLSpan>> roleMap;
        private int verb_depth;

	public SRLAnnotation()
	{
		//roles = new ArrayList<SRLSpan>();
		roleMap = new HashMap<SRLSpan.Type, ArrayList<SRLSpan>>();
	}
	
	public SRLAnnotation(SRLSpan verb)
	{
		super();
		setVerb(verb);
	}
	
	public void setVerb(SRLSpan verb_span)
	{
		verb = new SRLSpan(verb_span);
                // add verb to roles
                addRole(verb);
	}
	
	public SRLSpan getVerb()
	{
		return verb;
	}
	
	public void addRole(SRLSpan role)
	{
		//roles.add(role);
		//roleMap.put(role.getType(), role);
		if (roleMap.containsKey(role.getType()))
			roleMap.get(role.getType()).add(role);
		else
		{
			ArrayList<SRLSpan> r = new ArrayList<SRLSpan>();
			r.add(role);
			roleMap.put(role.getType(), r);
		}
	}
	
	public HashMap<SRLSpan.Type, ArrayList<SRLSpan>> getRoles()
	{
		return roleMap;
	}
	
	public ArrayList<SRLSpan> getRole(SRLSpan.Type t)
	{
		return roleMap.get(t);
	}

        public int getPredicateDepth()
        {
            return verb_depth;
        }

        public void setPredicateDepth(int depth)
        {
            verb_depth = depth;
        }
}