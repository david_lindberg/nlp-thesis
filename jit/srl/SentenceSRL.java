package jit.srl;

import jit.srl.*;
import java.util.ArrayList;

public class SentenceSRL
{
	private String[] tokenization;
	private ArrayList<SRLAnnotation> annotations;
	private String[] pos;
	
	public SentenceSRL(String[] tok, String[] pos, ArrayList<SRLAnnotation> ann)
	{
		tokenization = tok;
		annotations = ann;
		this.pos = pos;
	}
	
	public ArrayList<SRLAnnotation> getAnnotations()
	{
		return annotations;
	}
	
	public String[] getTokenization()
	{
		return tokenization;
	}
	
	public String[] getPOS()
	{
		return pos;
	}
}