package jit.srl.plugins;

import jit.srl.*;
import jit.srl.SRLSpan.Type;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.io.IOException;

public class SENNAPlugin
{
	public SentenceSRL getSRL(String sentence) throws IOException
	{
		String cmd = "";
		String os = System.getProperty("os.name").toLowerCase();
		if (os.indexOf("mac") >= 0)
			cmd = "sh srl-mac.sh "+sentence;
		else if (os.indexOf("nux") >= 0)
			cmd = "sh srl-linux64.sh "+sentence;
		else if (os.indexOf("win") >= 0)
			//batch file
			;
		else 
			System.out.println("unsupported OS");
		
		//String cmd = "sh srl.sh "+sentence;
                System.out.println(cmd);
		Process p = Runtime.getRuntime().exec(cmd);
		
		try
		{
			p.waitFor();
		}
		catch (InterruptedException e)
		{
			;//do nothing
		}
		BufferedReader srl_output = new BufferedReader(new InputStreamReader(p.getInputStream()));
//		System.out.println(srl_output.readLine());
		
		ArrayList<String> srl_lines = new ArrayList<String>();
		
		String line;
		while ( (line = srl_output.readLine()) != null)
		{
			if (line.equals(""))
				continue;
			else 
			{
//				System.out.println(line);
				srl_lines.add(line);
			}
		}
		
		srl_output.close();
		//System.out.println("size is " + srl_lines.size());
		
		for (int i = 0; i < srl_lines.size(); i++)
			System.out.println(srl_lines.get(i));
		//String[] sentence_srl_output = new String[srl_lines.size()];
		return parseSRLOutput(srl_lines);
		
		
		//ArrayList<SRLAnnotation> annotations = new ArrayList<SRLAnnotation>();
		//return annotations;
	}
	
	//utility function
	private static String[] splitString(String str, char delim)
	{
		String[] tokens = str.split("["+delim+"]+");
		return tokens;
	}
	
	private SentenceSRL parseSRLOutput(ArrayList<String> lines)
	{
		
		int rows = lines.size();
		int cols = splitString(lines.get(0), '\t').length;
		
		ArrayList<String> tokenization = new ArrayList<String>();
		ArrayList<String> pos = new ArrayList<String>();
//		System.out.println("will parse input:");
//		System.out.println();
		
//		for (int i = 0; i < lines.size(); i++)
//			System.out.println(lines.get(i));
		
//		System.out.println("size is " + lines.size());
//		System.out.println(rows + " tokens and " + (cols-3) + " srl annotations");
		ArrayList<SRLAnnotation> annotations = new ArrayList<SRLAnnotation>();

		// we're reading the senna output column by column starting from the 
		// rightmost column of the table
//		System.out.println(lines.get(0));
		for (int column = cols-1; column > 2; column--)
		{
			
//			System.out.println("col=" + column);
			SRLAnnotation this_annotation = new SRLAnnotation();
			// now we read down the column to identify the roles senna found and 
			// the word span of each role
			
			// initialize SRLSpan start and end
			int start = 0;
			int end = 0;

                        ///String type = "";
			Type type = Type.A0;

                        boolean isPlural = false;
			
			for (int row = 0; row < rows; row++)
			{

				//need to grab the entire line, which cuts across multiple columns
		//		System.out.println("having to split line -- " + lines.get(row));
				String[] tokens = splitString(lines.get(row), '\t');
				
				//first time through, grab the tokenization from the left-most column
				//and pos info from second column
				if (column == cols-1)
				{
					tokenization.add(tokens[0].replace(" ", ""));
					pos.add(tokens[1].replace(" ", ""));
				}
		//		System.out.println("has " + tokens.length + " tokens");
		/*		for (int i = 0; i < tokens.length; i++)
					System.out.println(tokens[i]+ " -- ");
		*/		// we want the 'column'th entry
				String col_token = tokens[column];
				col_token = col_token.replace(" ","");
				
				if (col_token.equals("0"))
					continue;

				
//				System.out.println("nonzero token " + col_token);
//				System.out.println("first " + col_token.charAt(0));
				if (col_token.charAt(0) == 'S') // spans only one word
				{
					// check if it's a verb first. Don't confuse V with ADV
					if (col_token.charAt(col_token.length()-1) == 'V' && col_token.charAt(col_token.length()-2) == '-')
					{
						if (this_annotation.getVerb() == null)
                                                {
							//this_annotation.setVerb(new SRLSpan(Type.V, row, row, false, false));
                                                        this_annotation.setVerb(new SRLSpan(Type.V, row, row, false, false));
                                                        this_annotation.setPredicateDepth(column-2);
                                                }
//						System.out.println("Verb at " + row);
					}
					else 
					{
						boolean subsumes = pos.get(row).contains("VB");
						///String[] split_token = splitString(col_token,'-');
                                                String suffix = col_token.substring(col_token.indexOf("-")+1);

                                                // replace '-' with '_'
                                                while (suffix.contains("-"))
                                                    suffix = suffix.replace("-","_");
						///REPACE THIS WITH GRABBING EVERYTHING AFTER S-, I-, or E-
						///String last = split_token[split_token.length-1];


                                                type = Type.valueOf(suffix);
						///type = suffix;
						//System.out.println(pos.get(row));
						isPlural = pos.get(row).contains("NNS") || pos.get(row).contains("NNPS") || pos.get(row).contains("CC");
						//System.out.println(isPlural);
						this_annotation.addRole(new SRLSpan(type, row, row, subsumes, isPlural));
//						System.out.println(type + " at " + row);
					}

				}
				// 'B' marks beginning of span
				else if (col_token.charAt(0) == 'B')
				{
					start = row; // beginning new span
					//determine which role this span fills
					///String[] split_token = splitString(col_token,'-');
                                        String suffix = col_token.substring(col_token.indexOf("-")+1);

                                        while (suffix.contains("-"))
                                            suffix = suffix.replace("-","_");
					///REPLACE THIS WITH GRABBING EVERYTHING AFTER S-, I-, or E-
					///String last = split_token[split_token.length-1];
					type = Type.valueOf(suffix);
					///type = suffix;

                                        //System.out.println(type);
//					if (type == Type.V)
//					System.out.println("starting " + type + " at " + start);
					
				}
				else if (col_token.charAt(0) == 'E')
				{
					
					//end of the current span
					if (type == Type.V)
                                        {
						this_annotation.setVerb(new SRLSpan(type, start, row, false, false));
                                                this_annotation.setPredicateDepth(column-2);
                                        }
					else 
					{
						boolean subsumes = false;
						// check if this span subsumes any verbs
						for (int r = start; r <= row; r++)
							if (pos.get(r).contains("VB"))
								subsumes = true;
								
						// check for any sort of plural pos tag between start and finish
						
						for (int t = start; t <= row; t++)
						{
							
							//System.out.println(pos.get(t));
							if (pos.get(t).equals("IN") && (t != start))
								break;
							else 
								isPlural = pos.get(t).contains("NNS") || pos.get(t).contains("NNPS");
							//System.out.println(isPlural);
						}
						//System.out.println(isPlural);
						this_annotation.addRole(new SRLSpan(type, start, row, subsumes, isPlural));
					}
//					if (type == Type.V)
//						System.out.println("ending " + type + " at " + row);
				}
			}
			annotations.add(this_annotation);
		}
		
		return new SentenceSRL(tokenization.toArray(new String[tokenization.size()]),
							   pos.toArray(new String[pos.size()]),annotations);
	}
}
