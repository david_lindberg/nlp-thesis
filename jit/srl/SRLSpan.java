package jit.srl;

import java.util.Map;

public class SRLSpan
{
	/// REMOVE THIS ENUM AND MAKE Type A SIMPLE STRING "AM-TMP, C-A1, ETC
	public enum Type
	{
		A0, A1, A2, A3, A4, A5, AA, V,
                C_A0, C_A1, C_A2, C_A3, C_A4, C_A5, C_AA,
                R_A0, R_A1, R_A2, R_A3, R_A4, R_A5, R_AA,
                AM_ADV, AM_CAU, AM_DIR, AM_DIS, AM_EXT, AM_LOC,
                AM_MNR, AM_MOD, AM_NEG, AM_PNC, AM_PRD, AM_REC,  AM_TMP,
                C_AM_ADV, C_AM_CAU, C_AM_DIR, C_AM_DIS, C_AM_EXT, C_AM_LOC,
                C_AM_MNR, C_AM_MOD, C_AM_NEG, C_AM_PNC, C_AM_PRD, C_AM_REC, C_AM_TMP,
                R_AM_ADV, R_AM_CAU, R_AM_DIR, R_AM_DIS, R_AM_EXT, R_AM_LOC,
                R_AM_MNR, R_AM_MOD, R_AM_NEG, R_AM_PNC, R_AM_PRD, R_AM_REC, R_AM_TMP;
	}

	
	private int start; //start index
	private int end; //end index 
	private Type type;
        ///private String type;
	private boolean hasNestedVerb; // contains a verb
	private boolean isPlural;
	
	public SRLSpan (Type t, int start, int end, boolean n, boolean p)
	{
		setType(t);
		setStart(start);
		setEnd(end);
		setNested(n);
		setPlural(p);
	}
	
	public SRLSpan (SRLSpan s)
	{
		type = s.getType();
		start = s.getStart();
		end = s.getEnd();
		hasNestedVerb = s.hasNestedVerb();
	}
	
	public SRLSpan (Type t)
	{
		setType(t);
	}
	
	public void setType(Type t)
	{
		type = t;
	}
	
	public void setStart(int index)
	{
		start = index;
	}
	
	public void setEnd(int index)
	{
		end = index;
	}
	
	public void setNested(boolean n)
	{
		hasNestedVerb = n;
	}
	
	public void setPlural(boolean p)
	{
		isPlural = p;
	}
	
	public int getStart()
	{
		return start;
	}
	
	public int getEnd()
	{
		return end;
	}
	
	public boolean hasNestedVerb()
	{
		return hasNestedVerb;
	}
	
	public int getSize()
	{
		return end - start + 1;
	}
	
	public Type getType()
	{
		return type;
	}
	
	public boolean isPlural()
	{
		return isPlural;
	}

}
