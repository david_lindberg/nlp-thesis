/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jit.glossary;

/**
 *
 * @author david
 */
import java.util.HashMap;
import java.util.ArrayList;

import jit.util.StringUtils;

public class Glossary
{
    private String[] keys;
    private HashMap<String, ArrayList<Integer>> in_terms_of;

    private int[] in_degree; // # in_degree[i] = number of terms that use key[i] in their gloss
    private int[] out_degree; // # out_degree[i] = number of terms key[i] is defined in terms of

    public Glossary(String file_stem)
    {
        loadGlossary(file_stem);
    }

    private void loadGlossary(String file_stem)
    {
        // load key, gloss
        // LATER
        // load in-terms-of file

        try
        {
            String[] ito = StringUtils.getFileContents(file_stem+".ito");
            keys = new String[ito.length];
            in_degree = new int[ito.length];
            out_degree = new int[ito.length];
            in_terms_of = new HashMap<String, ArrayList<Integer>>();

            for (int i = 0; i < ito.length; i++)
            {
                String[] entry = StringUtils.splitString(ito[i], '\t');
                keys[i] = entry[0];
                String key = entry[0];

                if (entry.length > 1)
                {
                    String[] ito_entries = StringUtils.splitString(entry[1], ' ');
                    for (String index : ito_entries)
                    {
                        int idx = Integer.decode(index);
                        // add idx to this key's in terms of list
                        if (!in_terms_of.containsKey(key))
                           in_terms_of.put(key, new ArrayList<Integer>());

                        in_terms_of.get(key).add(idx);
                        out_degree[i]++;
                        in_degree[idx]++;
                    }
                }
                else
                {
                    in_terms_of.put(key, null);
                }

            }
        }
        catch (Exception e)
        {
            System.err.println("Failed to load "+ file_stem+".ito");
            e.printStackTrace();

        }
    }

    public int[] getGlossaryAnnotations(String[] text)
    {
        // stem each word of 'text' and each word of each glossary key
        // then search 'text' for stemmed key

        ArrayList<Integer> indices = new ArrayList<Integer>();

        String stemmed_text = "";
        for (String word : text)
            stemmed_text += StringUtils.stem(word);

        stemmed_text = stemmed_text.trim();

        //System.out.println("stemmed text: "+stemmed_text);
        
        for (int i = 0; i < keys.length; i++)
        {
            String key = keys[i];
            //split
            String[] words = StringUtils.splitString(key, ' ');
            String stemmed_entry = "";
            for (String word : words)
                stemmed_entry += StringUtils.stem(word);

            stemmed_entry = stemmed_entry.trim();
            //System.out.println("looking for: "+ stemmed_entry);

            if (stemmed_text.toLowerCase().contains(stemmed_entry.toLowerCase()))
            {
                if (!indices.contains(i))
                    indices.add(i);
                System.out.println("found "+keys[i]);
            }
        }

        if (indices.size() > 0)
        {
            int[] indices_array = new int[indices.size()];
            for (int i = 0; i < indices.size(); i++)
                indices_array[i] = indices.get(i);

            return indices_array;

        }
        else
            return null;
    }

    private int indexOfTerm(String key)
    {
        for (int i = 0; i < keys.length; i++)
            if (keys[i].toLowerCase().equals(key.toLowerCase()))
                return i;
        return -1;
    }

    public void printGlossary()
    {
        for (int i = 0; i < keys.length; i++)
        {
            System.out.println("entry: "+ keys[i]);
            System.out.println("ito count: "+out_degree[i]);
            System.out.println("terms defined in terms of "+keys[i]+": "+in_degree[i]);
            if (out_degree[i] > 0)
            {
                System.out.println("in terms of:");

                for (int ito : in_terms_of.get(keys[i]))
                    System.out.println(keys[ito]);
            }
        }
    }

    private boolean hasEntry(String candidate)
    {
        // check for matched stems
        for (String key : keys)
            if (StringUtils.stem(key).equals(StringUtils.stem(candidate)))
                return true;

        return false;
    }

    public int inDegreeOf(int index)
    {
        return in_degree[index];
    }

    public int outDegreeOf(int index)
    {
        return out_degree[index];
    }
}
