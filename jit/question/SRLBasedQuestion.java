/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jit.question;

/**
 *
 * @author david
 */
public class SRLBasedQuestion
{
    private String text;
    private int token_count;
    private String[] tokenization;
    private int template_id;

    public SRLBasedQuestion(String txt, int tok_count, int tid)
    {
        text = txt;
        token_count = tok_count;
        template_id = tid;
    }

    public int getTemplateID()
    {
        return template_id;
    }

    public String getText()
    {
        return text;
    }

    public int getTokenCount()
    {
        return token_count;
    }
}
