/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jit.question;

/**
 *
 * @author david
 */
import jit.ranking.FeatureCollector.FeatureLabel;
import jit.ranking.Ranker;
import jit.question.SRLBasedQuestion;
import java.util.EnumMap;

public class FeatureAdornedSRLQuestion
{
    private SRLBasedQuestion q;
    private EnumMap<FeatureLabel, Double> features;
    private double score;

    public FeatureAdornedSRLQuestion(SRLBasedQuestion q, EnumMap<FeatureLabel, Double> ft, Ranker rank)
    {
        this.q = q;
        features = ft;
        score = rank.score(features);
    }

    public String getFeatureVectorString()
    {
        return features.toString();
    }
    
    public String getText()
    {
        return q.getText();
    }

    public double featureValue(FeatureLabel lbl)
    {
        return features.get(lbl);
    }

    public int featureCount()
    {
        return features.size();
    }

    public String getFeatureValueString()
    {
        String str = "";
        for (FeatureLabel ft : features.keySet())
        {
            str += features.get(ft)+" ";
        }
        return str;
    }

    public int getTemplateID()
    {
        return q.getTemplateID();

    }

    public double getScore()
    {
        return score;
    }
}
