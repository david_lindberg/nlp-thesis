package jit.util;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.List;
import edu.northwestern.at.utils.corpuslinguistics.lemmatizer.EnglishLemmatizer;
import edu.northwestern.at.utils.corpuslinguistics.lemmatizer.PorterStemmerLemmatizer;
import edu.northwestern.at.utils.corpuslinguistics.tokenizer.DefaultWordTokenizer;

public class StringUtils
{
    public static String[] getFileContents(String filename)
        throws IOException, FileNotFoundException
    {
        File file = new File(filename);
        StringBuilder contents = new StringBuilder();

        BufferedReader input = new BufferedReader(new FileReader(file));

        try {
            String line = null;

            while ((line = input.readLine()) != null) {
                contents.append(line);
                contents.append(System.getProperty("line.separator"));
            }
        } finally {
            input.close();
        }

        return contents.toString().split(System.getProperty("line.separator"));
    }
	
	public static String concatStringArray(String[] array, String delim)
	{
		String out = array[0];
		if (array.length > 1)
			for (int i = 1; i < array.length; i++)
			{
				if (array[i].equals(".") || array[i].equals(",") || array[i].equals("?") || array[i].equals("'s"))
					out += array[i];
				else 
					out += delim + array[i];
			}
		return out;
	}

	public static String[] splitString(String str, char delim)
	{
		String[] tokens = str.split("["+delim+"]+");
		return tokens;
	}
	
	public static String genericPreprocess(String str)
	{ // called on role text

            String tmp_str = "";
            if (str.indexOf(':') != -1)
		str = str.substring(0,str.indexOf(':'));

            // remove parentheticals

            while (str.indexOf('(') != -1)
            {
                tmp_str = str.substring(0,str.indexOf('('));

                // SENNA sometimes doesn't pick up ')'
                if (str.indexOf(')') == -1)
                    str = tmp_str;
                else
                    str = tmp_str + " " + str.substring(str.indexOf(')')+1);
            }

            // remove any stray closing parentheses
            while (str.indexOf(')') != -1)
            {
                if (str.indexOf(')') == str.length()-1)
                    str = str.substring(0, str.indexOf(')'));
                else
                    str = str.substring(0, str.indexOf(')'))+ str.substring(str.indexOf(')')+1);
            }
            // IDEA: (removing subordinate clauses)
            // if role text has only one comma which is NOT followed by "and/or"
            // take substring up to comma
            
            return str;
	}
	
	public static String genericPostprocess(String str)
	{
            //called on question text
            /*
		str = str.replaceAll(" ,", ",");
		str = str.replaceAll("\\( ", "(");
		str = str.replaceAll(". ", ".");
		str = str.replaceAll(" ;", ";");
		str = str.replaceAll(" :", ":");
		*/

                str = str.replace(",?", "?");
                str = str.replace(" ?", "?");
                str = str.replace("  ", " ");
                str = str.replace(" .", ".");
                str = str.replace("~", "-");
                str = str.replace(" - ","-");

                // uppercase

                str = Character.toUpperCase(str.charAt(0)) + str.substring(1);


		return str;
	}

    public static int countOccurrences(String str, char c)
    {
        int count = 0;
        for (int i=0; i < str.length(); i++)
        {
            if (str.charAt(i) == c)
            {
                count++;
            }
        }
        return count;
    }

    private static String lemmatize(String str)
    {
        String out = "";
        try
        {
            EnglishLemmatizer lemmatizer = new EnglishLemmatizer();
            out = lemmatizer.lemmatize(str);
        }
        catch (Exception e)
        {;}
         return out;
    }

    private static String stem(ArrayList<String> str)
    {
        String output = new String();
        try
        {
            //EnglishLemmatizer lemmatizer = new EnglishLemmatizer();
            PorterStemmerLemmatizer lemmatizer = new PorterStemmerLemmatizer();
            for (String token : str)
                output += lemmatizer.lemmatize(token)+" ";
        }
        catch(Exception e)
        {
            System.out.println("err");
        }

        return output;
    }

    public static String stem(String str)
    {
        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add(str);
        return stem(tmp);
    }

    public static String[] tokenize(String str)
    {
        DefaultWordTokenizer tokenizer = new DefaultWordTokenizer();
        
        List<String> words = tokenizer.extractWords(str);
        
        return words.toArray(new String[words.size()]);
    }

}