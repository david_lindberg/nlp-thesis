package jit.util;

import jit.util.StringUtils;

import edu.northwestern.at.utils.corpuslinguistics.sentencesplitter.DefaultSentenceSplitter;
import org.apache.poi.xwpf.extractor.*;
import org.apache.poi.xwpf.usermodel.*;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

public class FormatConverter
{
	public static String[] getLines(String filename, String format) throws IOException, FileNotFoundException
	{
		File file = new File(filename);
		String[] lines = null;
                DefaultSentenceSplitter splitter = new DefaultSentenceSplitter();
                List<List<String>> tmp_lines_tok;; // tokenized lines
                String text = "";
                ArrayList<String> tmp_lines = new ArrayList<String>();

		if (format.toLowerCase().equals("docx"))
		{
			XWPFDocument doc = new XWPFDocument(new FileInputStream(file));
			XWPFWordExtractor wordExtractor = new XWPFWordExtractor(doc);
			text = wordExtractor.getText();
			//PrintWriter txt = new PrintWriter(args[0]+".txt");
			//txt.println(text);
			//txt.close();

                        /*
			ArrayList<String> tmp_lines = new ArrayList<String>();
			String[] main_blocks = StringUtils.splitString(text,'\n');
			if (main_blocks.length > 1)
			{
				tmp_lines.add(main_blocks[0]);
				for (int l = 1; l < main_blocks.length; l++)
				{
					String[] tmp = StringUtils.splitString(main_blocks[l],'.');
					for (String t : tmp)
						tmp_lines.add(t+".");
				}
			}
			lines = tmp_lines.toArray(new String[tmp_lines.size()]);

                        */
		}
		else 
			System.out.println("ERROR: unrecognized input file format");

                tmp_lines_tok = splitter.extractSentences(text);

                for (List<String> tok : tmp_lines_tok)
                {
                    String line = StringUtils.concatStringArray(tok.toArray(new String[tok.size()])," ");
                    tmp_lines.add(line);
                }

		return tmp_lines.toArray(new String[tmp_lines.size()]);
	}
}