package jit.generator;

import jit.util.StringUtils;
import jit.generator.Template;
import jit.srl.*;
import jit.sentence.Sentence;
import jit.ranking.FeatureCollector;
import jit.ranking.Ranker;
import jit.ranking.QuestionComparator;
import jit.question.SRLBasedQuestion;
import jit.question.FeatureAdornedSRLQuestion;
import jit.glossary.Glossary;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.PriorityQueue;

public class Generator
{
	private ArrayList<Template> templates;
        //private int[] template_use_count;
	private int question_count;
        private Glossary glossary;
        private Ranker ranker;

	public Generator(String template_file, String feature_wt_file)
	{
		templates = new ArrayList<Template>();
                ranker = new Ranker(getDoubleArrayFromFile(feature_wt_file));
		try
		{
			loadTemplatesFromFile(template_file);
		}
		catch (IOException e)
		{
			;
		}


	}

        public int templateCount()
        {
            return templates.size();
        }

        public void setGlossary(Glossary g)
        {
            glossary = g;
        }

	public void loadTemplatesFromFile(String filename) throws IOException, FileNotFoundException
	{
                int id = -1;
		String[] template_txt = StringUtils.getFileContents(filename);
		//System.out.println(template_txt.length);
		for (String str : template_txt)
		{
			if (str.charAt(0) == '#')
				continue;
			else 
                        {
				templates.add(new Template(str,++id));
                                
                        }
		}
                //template_use_count = new int[templates.size()];

	}

        public double[] getDoubleArrayFromFile(String filename)
        {
            String[] weight_str;
            try
            {
                weight_str = StringUtils.getFileContents(filename);

            }
            catch(Exception e)
            {
                weight_str = new String[0];
            }

            double[] weights = new double[weight_str.length];

            for (int i = 0; i < weight_str.length; i++)
            {
                weights[i] = Double.parseDouble(weight_str[i]);
            }
            return weights;
        }

	// generateQuestions
	// args: tokenization - tokenization of the source sentence
	//       srl - SentenceSRL object for the source sentence
	/*
	public String[] generateQuestions(String[] tokenization, SentenceSRL srl)
	{
		ArrayList<String> questions = new ArrayList<String>();
		for (SRLAnnotation ann : srl.getAnnotations())
		{
			for (Template t : templates)
			{
				if (t.meetsAllPrereqs(ann))
				{
					//System.out.println("template match");
					questions.add(t.fillSlots(tokenization,ann));
				}
			//	else 
			//		System.out.println("template mismatch");
			}
		}
		
		return questions.toArray(new String[questions.size()]);
	}
	*/
	public PriorityQueue<FeatureAdornedSRLQuestion> generateQuestions(Sentence sentence, FeatureCollector fc)
	{

		SentenceSRL srl = sentence.getSRL();
		String[] tok = srl.getTokenization();
		String[] pos = sentence.getPOS().toArray(new String[sentence.getPOS().size()]);
		String[] lemma = sentence.getLemma().toArray(new String[sentence.getLemma().size()]);
		String[] ner = sentence.getNER().toArray(new String[sentence.getNER().size()]);


		ArrayList<FeatureAdornedSRLQuestion> adorned_questions = new ArrayList<FeatureAdornedSRLQuestion>();
                ArrayList<String> questions = new ArrayList<String>();

                PriorityQueue<FeatureAdornedSRLQuestion> question_queue = new PriorityQueue(1,new QuestionComparator());

		for (SRLAnnotation ann : srl.getAnnotations())
		{
			//for (Template t : templates)
			for (int i = 0; i < templates.size(); i++)
                        {
                            Template t = templates.get(i);
				if (t.meetsAllPrereqs(pos,lemma,ner,ann))
				{
                                    try
                                    {
                                        // call to fillSlots should return SRLBasedQuestion
					SRLBasedQuestion question = t.fillSlots(pos, lemma, ner, tok, ann);
					//questions.add(t.fillSlots(pos, lemma, ner, tok, ann));
					if (!questions.contains(question.getText()))
                                        {

						//questions.add(question.getText());
                                                //template_use_count[i]++;
                                                fc.collectFeatures(sentence, question, t, ann, glossary);

                                                //adorned_questions.add(new FeatureAdornedSRLQuestion(question.getText(),fc.getFeatures(),ranker));
                                                question_queue.add(new FeatureAdornedSRLQuestion(question,fc.getFeatures(),ranker));
                                                questions.add(question.getText());
                                                fc.clearFeatures();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }
				}
			}
		}
		System.out.println("adorned question count: "+adorned_questions.size());
		//return adorned_questions.toArray(new FeatureAdornedSRLQuestion[questions.size()]);
	       return question_queue;
        }
//        public int[] getTemplateUsageCount()
//        {
//            return template_use_count;
//        }
}