package jit.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.HashMap;
import edu.northwestern.at.utils.corpuslinguistics.lemmatizer.*;
import edu.northwestern.at.utils.corpuslinguistics.inflector.conjugator.EnglishConjugator;
import edu.northwestern.at.utils.corpuslinguistics.inflector.VerbTense;
import edu.northwestern.at.utils.corpuslinguistics.inflector.Person;
import jit.srl.SRLSpan;
import jit.srl.SRLAnnotation;
import jit.util.StringUtils;
import jit.question.SRLBasedQuestion;



public class Template
{
	private static String pres = "PRESENT";
	private static String prespart = "PRESENT_PARTICIPLE";
	private static String past = "PAST";
	private static String pastpart = "PAST_PARTICIPLE";
	private static String perf = "PERFECT";
	private static String pastperf = "PAST_PERFECT";
	private static String pastperfpart = "PAST_PERFECT_PARTICIPLE";
	
	private static String fps = "FIRST_PERSON_SINGULAR";
	private static String sps = "SECOND_PERSON_SINGULAR";
	private static String tps = "THIRD_PERSON_SINGULAR";
	private static String fpp = "FIRST_PERSON_PLURAL";
	private static String spp = "SECOND_PERSON_PLURAL";
	private static String tpp = "THIRD_PERSON_PLURAL";

        private int id;
	private String text;
	private ArrayList<SRLSpan.Type> slots;
	private ArrayList<SRLSpan.Type> other_prereqs;
	private HashMap<SRLSpan.Type, String> slot_options;
	private HashMap<SRLSpan.Type, String> other_prereqs_options;
	
	public Template(String txt, int id)
	{
                this.id = id;
		String[] split_txt = StringUtils.splitString(txt,'#');
		slots = new ArrayList<SRLSpan.Type>();
		other_prereqs = new ArrayList<SRLSpan.Type>();
		slot_options = new HashMap<SRLSpan.Type, String>();
		other_prereqs_options = new HashMap<SRLSpan.Type, String>();
		//System.out.println("setting text to: " + split_txt[0]);
		setText(split_txt[0]);
		parseText(split_txt);
	}

        public boolean hasNullRequirement(SRLSpan.Type t)
        {
            return other_prereqs_options.get(t).contains(" null");
        }
        
        public ArrayList<SRLSpan.Type> getSlots()
        {
            return slots;
        }

        public ArrayList<SRLSpan.Type> getOtherPrereqs()
        {
            return other_prereqs;
        }
	private void setText(String txt)
	{
		text = txt;
	}
	
	private void parseText(String[] txt)
	{
		// identify the slots to be filled
		for (SRLSpan.Type t : SRLSpan.Type.values())
		{
			if ( txt[0].contains("["+ t.name()))
			{
			//	System.out.println("adding slot " + t.name());
				slots.add(t);
				
				//check for options specified for this slot
				int start = txt[0].indexOf(t.name(),0);
				String substr = txt[0].substring(start, txt[0].indexOf(']', start));
				//substr.replace(t.name(), ""); // don't need to store the type in the options string
				if (substr == null)
					substr = "";
				slot_options.put(t, substr);
			}
			
			// check if type t is a non-fillable prereq
			if ( txt.length > 1 )
				if (txt[1].contains("["+ t.name()))
				{
					other_prereqs.add(t);
					int start = txt[1].indexOf(t.name(),0);
					String substr = txt[1].substring(start, txt[1].indexOf(']',start));
					//System.out.println("adding non slot options for " +t.name());
					//System.out.println(substr);
					if (substr == null)
						substr = "";
					other_prereqs_options.put(t, substr);
				}
		}
		
	}
	
	public boolean meetsAllPrereqs(String[] pos, String[] lemma, String[] ner, SRLAnnotation ann)
	{
		//System.out.println(ann.getVerb());
		if (hasMatchingSlots(pos,lemma,ner,ann) && meetsNonSlotPrereqs(pos,lemma,ner,ann))
			return true;
		else 
			return false;
		
	}
	
	private boolean meetsNonSlotPrereqs(String[] pos, String[] lemma, String[] ner, SRLAnnotation ann)
	{
		boolean match = true;
		for (SRLSpan.Type t : other_prereqs)
		{
			String opt = other_prereqs_options.get(t);

			if (match == false)
				break;
                        else if(t.name().equals("V"))
			{
				if (ann.getVerb() == null)
					continue;
				// check verb options
				if (opt.contains("!be"))
				{
					if (lemma[ann.getVerb().getStart()].equals("be"))
						//match = false;
						return false;
					//System.out.println("not 'be' verb");
				}
				else if (opt.contains("be"))
				{
					if (!lemma[ann.getVerb().getStart()].equals("be"))
						//match = false;
						return false;
				}
				if (opt.contains("!have"))
				{
					if (lemma[ann.getVerb().getStart()].equals("have"))
						//match = false;
						return false;
				}
			}
			else if (ann.getRole(t) == null)
			{
				if (!opt.contains("null"))
					match = false;
			}
			
			else 
			{
			// check options
                                if (opt.contains("null"))
                                    return false;
                            	if (opt.contains("!nv") && ann.getRole(t).get(0).hasNestedVerb())
					//match = false;
					return false;
				if (opt.contains("dur")) // duration, check NER
				{
					//System.out.println("looking for DURATION");
					boolean found = false;
					for (int i = ann.getRole(t).get(0).getStart(); i <= ann.getRole(t).get(0).getEnd(); i++)
						if (ner[i].equals("DURATION"))
							found = true;
					
					if (!found)
						match = false;
				}

                                if (opt.contains("date"))
				{
					boolean found = false;
					for (int i = ann.getRole(t).get(0).getStart(); i < ann.getRole(t).get(0).getEnd(); i++)
						if (ner[i].equals("DATE"))
							found = true;

					match = found;

                                        if (opt.contains("!date"))
                                        {
                                            match = !found;

                                            /*
                                            System.out.println(opt);
                                            for (String n : ner)
                                                System.out.println(n);
                                             * 
                                             */
                                        }


				}

				if (opt.contains("loc"))
				{
					boolean found = false;
					for (int i = ann.getRole(t).get(0).getStart(); i < ann.getRole(t).get(0).getEnd(); i++)
						if (ner[i].equals("LOCATION"))
							found = true;
					
					if (!found)
						match = false;
					
				}
				
				if (opt.contains("comp"))
				{
					// look for pos tags indicating a comparison
					
					// check pos for adverb (RB) next to adjective (JJ)
					boolean found = false;
					for (int i = ann.getRole(t).get(0).getStart(); i<ann.getRole(t).get(0).getEnd();i++)
					{
						//System.out.println(pos[i] + ",,," + pos[i+1]);
						if(pos[i].equals("RB") && pos[i+1].equals("JJ"))
						{
							//System.out.println("TRUE!!!!!!!!!!!");
							found = true;
						}
					}
					
					// others to be added
					match = found;
				}
			}
		}
		
		return match;
	}
	
	private boolean hasMatchingSlots(String[] pos, String[] lemma, String[] ner, SRLAnnotation annotation)
	{
		boolean match = true;
		for (SRLSpan.Type t : slots)
		{
			if (t.name().equals("V"))
			{
				if (annotation.getVerb() == null)
					continue;
				// check verb options
				String opt = slot_options.get(t);
				if (opt.contains("!be"))
				{
					if (lemma[annotation.getVerb().getStart()].equals("be"))
						//match = false;
						return false;
					//System.out.println("not 'be' verb");
				}
				else if (opt.contains("be"))
				{
					if (!lemma[annotation.getVerb().getStart()].equals("be"))
						//match = false;
						return false;
				}
				if (opt.contains("!have"))
				{
					if (lemma[annotation.getVerb().getStart()].equals("have"))
						//match = false;
						return false;
				}
			}
			//System.out.println("looking for " + t.name());
			else if (annotation.getRole(t) == null)
			{
			//	System.out.println("didn't find " + t.name());
				//match = false;
				return false;
			}
			else 
			{
				// check if slot options match too
				String opt = slot_options.get(t);
				if (opt.contains("!nv") && annotation.getRole(t).get(0).hasNestedVerb())
					//match = false;
					return false;
				if (opt.contains("ne"))
				{
					boolean found = false;
					for (int i = annotation.getRole(t).get(0).getStart(); i <= annotation.getRole(t).get(0).getEnd(); i++)
						if (!(ner[i].equals("O")))
							return true;
					
					
						//match = false;
                                        return false;
				}
				if (opt.contains("misc"))
				{
					boolean found = false;
					for (int i = annotation.getRole(t).get(0).getStart(); i <= annotation.getRole(t).get(0).getEnd(); i++)
						if (i < ner.length)
							if (ner[i].equals("MISC"))
								found = true;
					
					if (!found)
						//match = false;
						return false;
				}
				if (opt.contains("!comma"))
				{
					boolean found = false;
					for (int i = annotation.getRole(t).get(0).getStart(); i< annotation.getRole(t).get(0).getEnd();i++)
						if (pos[i].equals(","))
							//match = false;
							return false;
				}
				
				if (opt.contains("comp"))
				{
					// check pos for adverb (RB) next to adjective (JJ)
					boolean found = false;
					for (int i = annotation.getRole(t).get(0).getStart(); i<annotation.getRole(t).get(0).getEnd()-1;i++)
						if(pos[i].equals("RB") && pos[i+1].equals("JJ"))
							found = true;
					
					//match = found;
					if (!found)
						return false;
				}
				
				if (opt.contains("plural"))
                                {
					match = annotation.getRole(t).get(0).isPlural();
                                        //System.out.println("plural: " + match);
                                }
				else if (opt.contains("singular"))
                                {
					match = !annotation.getRole(t).get(0).isPlural();
                                        //System.out.println("singular: " + match);
                                }
				
				if (!match)
					return false;
				/*
				if (opt.contains("plural"))
				{
					//System.out.println("checking for plurality");
					boolean found=false;
					
					
					for (int i = annotation.getRole(t).get(0).getStart(); i<annotation.getRole(t).get(0).getEnd();i++)
					{
						//System.out.println(pos[i]);
						if (pos[i].equals("IN")) // don't search prepositional clauses
							break;
						else if (pos[i].equals("CC"))
						{
							found = true;
							break;
						}
						else if (pos[i].equals("NNS") || pos[i].equals("NNPS"))
						{
							found = true;
							break;
						}
					}
					match=found;
					//System.out.println("plural "+match);
				}
				else if (opt.contains("singular"))
				{
					//System.out.println(opt);
					boolean found=false;
					for (int i = annotation.getRole(t).get(0).getStart(); i<annotation.getRole(t).get(0).getEnd();i++)
					{
						//System.out.println(pos[i]);
						if (pos[i].equals("IN")) // don't search prepositional clauses
							break;
						else if (pos[i].equals("CC"))
						{
							found = false;
							break;
						}
						else if (pos[i].equals("NN") || pos[i].equals("NNP"))
						{
							found = true;
						}
						else if (pos[i].equals("NNS") || pos[i].equals("NNPS"))
						{
							found = false;
							break;
						}
					}
					//System.out.println("singular: "+found);
					match=found;
				}
				
				*/
				
			}
			//else 
			//	System.out.println("found " + t.name());
		}
		//System.out.println("returning " + match);
		return match;
	}
	
	public SRLBasedQuestion fillSlots(String[] pos, String[] lemma, String[] ner, String[] tokenization, SRLAnnotation ann) throws Exception
	{
		//System.out.println("processing template: ");
		//System.out.println(text);

                SRLBasedQuestion question;
		EnglishConjugator conj = new EnglishConjugator();
		EnglishLemmatizer lemm = new EnglishLemmatizer();
		/*
		for (VerbTense v : VerbTense.values())
			for (Person c : Person.values())
				System.out.println(v.toString() + " " + c.toString() + ": "+ conj.conjugate(lemma[ann.getVerb().getStart()],v,c));
		*/
		VerbTense tense = null;
		Person person = null;
		
		String str = text;
		//we may need to change verb conjugation, so make copy of tokenization
		String[] tok_copy = new String[tokenization.length];
		System.arraycopy(tokenization,0,tok_copy,0,tokenization.length);
		
		//System.out.println(text);
		//Iterator<SRLSpan.Type> itr = ann.getRoles().keySet().iterator();
		for (SRLSpan.Type t : slots)
		{
			String role_text = new String();
			ArrayList<SRLSpan> s;
			//ArrayList<SRLSpan> s = ann.getRole(itr.next());
			String opt = slot_options.get(t);
			if (t.name().equals("V"))
			{
				
				// in a rare case, an SRLAnnotation won't have a verb
				if (ann.getVerb() == null)
					return null;
                                else if (!pos[ann.getVerb().getStart()].contains("VB"))
                                    return null; //sometimes a non-verb is somehow picked up as a verb, so for now, skip such cases
				
				
				if (opt.contains("lemma"))
				{
					// replace verb with lemma
					tok_copy[ann.getVerb().getStart()] = lemma[ann.getVerb().getStart()];
				}
				else if (opt.contains("pres"))
				{
					if (opt.contains("prespart"))
					{
						tense = VerbTense.valueOf(prespart);
					}
					else 
					{
						tense = VerbTense.valueOf(pres);
					}

				}
				else if (opt.contains("past"))
				{
					if (opt.contains("pastpart"))
					{
						tense = VerbTense.valueOf(pastpart);
					}
					else if (opt.contains("pastperf"))
					{
						if (opt.contains("pastperfpart"))
						{
							tense = VerbTense.valueOf(pastperfpart);
						}
						else 
						{
							tense = VerbTense.valueOf(pastperf);
						}
					}
					else 
					{
						tense = VerbTense.valueOf(past);
					}
					
				}
				else if (opt.contains("perf"))
				{
					tense = VerbTense.valueOf(perf);
				}

				
				// get person modifier (if any)
				if (opt.contains("fp"))
				{
					if (opt.contains("fps"))
						person = Person.valueOf(fps);
					else if (opt.contains("fpp"))
						person = Person.valueOf(fpp);
				}
				else if (opt.contains("sp"))
				{
					if (opt.contains("sps"))
						person = Person.valueOf(sps);
					else if (opt.contains("fpp"))
						person = Person.valueOf(spp);
				}				
				else if (opt.contains("tp"))
				{
					if (opt.contains("tps"))
						person = Person.valueOf(tps);
					else if (opt.contains("tpp"))
						person = Person.valueOf(tpp);
				}
							 
				if (tense != null && person != null)
                                {

                                    
                                    
                                     //   System.out.println("verb: "+ tok_copy[ann.getVerb().getStart()]);
                                     //   System.out.println("lemma: "+lemm.lemmatize(tok_copy[ann.getVerb().getStart()]));
                                    
                                    tok_copy[ann.getVerb().getStart()] = conj.conjugate(lemm.lemmatize(tok_copy[ann.getVerb().getStart()]),tense,person);
                                }
                                s = new ArrayList<SRLSpan>();
				s.add(ann.getVerb());
			}
			else 
			{
				s = ann.getRole(t);
				
				if (opt.contains("-lpp"))
				{
                                    int i = 0;
                                    while (pos[s.get(0).getStart()+i].equals("IN") || pos[s.get(0).getStart()+i].equals("TO"))
                                    {
                                        tok_copy[s.get(0).getStart()+i] = "";
                                        i++;
                                    }
                                    /*
					// check pos tag of first token
					String first_token = tok_copy[s.get(0).getStart()];
					String first_pos = pos[s.get(0).getStart()];
					if (first_pos.equals("IN") || first_pos.equals("TO"))
						tok_copy[s.get(0).getStart()] = "";
                                        */
				}

                                //remove leading determiners?
                                else if (opt.contains("-ldt"))
                                {
                                    int i = 0;
                                    while (pos[s.get(0).getStart()+i].equals("DT"))
                                    {
                                        tok_copy[s.get(0).getStart()+i] = "";
                                        i++;
                                    }
                                    /*
                                    String first_token = tok_copy[s.get(0).getStart()];
                                    String first_pos = pos[s.get(0).getStart()];
                                    if (first_pos.equals("DT"))
                                        tok_copy[s.get(0).getStart()] = "";

                                    */
                                }
                                else if (opt.contains("-tpp"))
                                {
                                    // remove trailing prepositional phrases
                                    boolean truncate = false; // start truncating only after TO or IN pos tag found
                                    for (int i = 0; i < pos.length; i++)
                                    {
                                        if (pos[i].equals("TO") || pos[i].equals("IN"))
                                        {
                                            truncate = true;
                                            tok_copy[i] = "";
                                        }
                                        else if (truncate)
                                            tok_copy[i] = "";
                                    }
                                }
			}
			role_text = (s.get(0).getStart() == s.get(0).getEnd()) ?
							tok_copy[s.get(0).getStart()] :
							StringUtils.concatStringArray(Arrays.copyOfRange(tok_copy,s.get(0).getStart(),s.get(0).getEnd()+1)," ");
			
			role_text = StringUtils.genericPreprocess(role_text);
			//role_text = role_text.replaceAll("  ", " ");
			
			// lowercase only if necessary
                        // get NE value of first word
                        String ne = ner[s.get(0).getStart()];
/*                        System.out.println("NE " + ne);
                        System.out.println(role_text);
                        System.out.println(s.get(0).getStart());
 *  
*/
                            System.out.println(role_text + " -- ne: " + ne +", " + s.get(0).getStart());

                        if (s.get(0).getStart() == 0 && ((ne.equals("O")) || ne.equals("NUMBER")) || ne.equals("PERCENT") || ne.equals("DATE"))
                        {
                            //System.out.println("must lc");
                            role_text = Character.toLowerCase(role_text.charAt(0))+role_text.substring(1);
                           // System.out.println("lc: " + role_text);
                        }
		///	role_text = role_text.toLowerCase();
		//	System.out.println("Role text: ");
		//	System.out.println(role_text);
		//	System.out.println("prior to replace: ");
		//	System.out.println(str);
		//	String replace_string = "["+slot_options.get(s.get(0).getType())+"]";
		//	System.out.println("replacing text: " + replace_string);
		//	System.out.println("replacing: " + s.get(0).getType().name() + " with " + role_text);
			str = str.replaceAll("\\["+s.get(0).getType().name()+ ".*?\\]", role_text);
			str = str.replaceAll("[ ]{2}"," ");


			//str = StringUtils.genericPostprocess(str);
		
		}
                question = new SRLBasedQuestion(StringUtils.genericPostprocess(str), str.split(" ").length, id);
		return question;
	}

        public boolean hasSlot(SRLSpan.Type t)
        {
            return slots.contains(t);
        }

        public boolean hasNonSlotPrereq(SRLSpan.Type t)
        {
            return other_prereqs.contains(t);
        }
}