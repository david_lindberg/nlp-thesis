package jit.anaphora;

import jit.util.StringUtils;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Properties;
import java.util.HashMap;
import java.util.ArrayList;

import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.util.*;
import edu.stanford.nlp.ling.CoreAnnotations.*;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefChain.CorefMention;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.*;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.*;

import java.io.FileNotFoundException;
import java.io.IOException;

public class PronounResolver
{
	public static String[] resolve(String[] lines) throws FileNotFoundException, IOException, InterruptedException
	{
                
		PrintWriter parse_out = new PrintWriter("emPronoun/tmp/tmp0.txt");
		Properties p = new Properties();
		p.put("annotators","tokenize, ssplit, parse");
		StanfordCoreNLP pipe = new StanfordCoreNLP(p);
		
		
		// parse each line and send output to parse_out
		for (String line : lines)
		{
			if (line.length() < 3)
				continue;
			Annotation tree_doc = new Annotation(line);
			pipe.annotate(tree_doc);
			
			CoreMap sentence = tree_doc.get(SentencesAnnotation.class).get(0);
			
			parse_out.println(sentence.get(TreeAnnotation.class).toString());
		}
		parse_out.close();

		String cmd = "";
		String os = System.getProperty("os.name").toLowerCase();
                System.out.println(os);
		if (os.indexOf("mac") >= 0)
			cmd = "sh pronouns-osx.sh";
		else if (os.indexOf("nux") >= 0)
			cmd = "sh pronouns-linux64.sh";
		else if (os.indexOf("win") >= 0)
			//batch file
			;
		else
			System.out.println("unsupported OS");
		Process prn_res = Runtime.getRuntime().exec(cmd/*"sh pronouns.sh*/+" output");
		prn_res.waitFor();
		
		// read tree strings back into one string
		String trees = "";
		BufferedReader br = new BufferedReader(new FileReader("emPronoun/tmp/output"));
		String tree;
		while ((tree = br.readLine()) != null)
			trees+= tree+ '\n';
		
		HashMap<String, String> anaphora = new HashMap<String, String>();
		
		int start_index = 0;
		int found_index = trees.indexOf('#',start_index);
		ArrayList<String> repl = new ArrayList<String>();
		while (found_index != -1)
		{
			//			if (found_index > 0)
			//				System.out.println(trees.substring(found_index, trees.indexOf(' ', found_index)));
			
			start_index = found_index+1;
			// get subtree
			int string_start;
			for (string_start = found_index; trees.charAt(string_start)!='('; string_start--);
			
			int open_br = 1;
			int string_end;
			String number=new String();
			boolean found_number = false;
			for (string_end = found_index; open_br != 0; )
			{
				if (trees.charAt(string_end)==' ' && !found_number)
				{
					number = trees.substring(found_index, string_end);
					found_number=true;
				}
				if (trees.charAt(string_end)==')')
					open_br--;
				else if (trees.charAt(string_end)=='(')
					open_br++;
				
				string_end++;
			}
			
			String subtree = trees.substring(string_start, string_end);
			if (anaphora.get(number) == null)
				anaphora.put(number, subtree);
			else
			{ 
				//				System.out.println("replacing: "+ subtree + " with " + anaphora.get(number));
				trees = trees.replace(subtree, anaphora.get(number));
			}
			
			
			// end get subtree
			
			//			System.out.println(number + ": " + subtree);
			found_index = trees.indexOf('#',start_index);
		}
		
		lines = StringUtils.splitString(trees,'\n');
		for (int l = 0; l < lines.length; l++)
		{
			if (lines[l].length() < 3)
				continue;
			Tree t = Tree.valueOf(lines[l]);
			String sentence = "";
			for (Tree subtree : t)
				if (subtree.isLeaf())
				{
					// de-hyphenate names
					String val = subtree.label().value();
					
					int hyph_idx = val.indexOf('-');
					while (hyph_idx > 0 && hyph_idx < val.length()-1)
					{
						if (val.charAt(hyph_idx-1) != ' ' && val.charAt(hyph_idx+1) != ' ')
						{
							System.out.println(val);
							val = val.substring(0,hyph_idx)+"~"+val.substring(hyph_idx+1);
							System.out.println(val);
						}
						hyph_idx = val.indexOf('-',hyph_idx+1);
					}
					/*	while (val.matches("\\[a-zA-Z\\]-\\[a-zA-Z\\]"))
					 {
					 int hyphen_idx = val.indexOf('-',0);
					 
					 if (val.charAt(hyphen_idx-1) != ' ' && val.charAt(hyphen_idx+1) != ' ')
					 val = val.substring(0,hyphen_idx)+" "+val.substring(hyphen_idx+1);	
					 }*/
					sentence += val+" ";
				}
			///sentence += subtree.label().value()+" ";
			//String value = Tree.value(lines[l]);
			sentence = sentence.replaceAll("-LRB- ", "(");
			sentence = sentence.replaceAll("-RRB- ", ")");
			sentence = sentence.replaceAll("/"," and ");
			sentence = sentence.replaceAll("\\\\", "");
			lines[l] = sentence;
			System.out.println(lines[l]);
		}
		
		return lines;
	}
}